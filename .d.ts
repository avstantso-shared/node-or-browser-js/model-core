declare module 'ajv-base64' {
  function addBase64(ajv: Ajv): unknown;

  export = addBase64;
}
