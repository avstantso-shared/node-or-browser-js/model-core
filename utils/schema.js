const path = require('path');
const fs = require('fs');
const { writeSourceFile } = require('@avstantso/node-js--code-gen');
const { CodeGen } = require('@avstantso/node-js--model-schemas-gen');

const scriptName = 'schema';

const [forCode, forTest] = (() => {
  const argv = process.argv.slice(2);
  return [argv.indexOf('-c') >= 0, argv.indexOf('-t') >= 0];
})();

const forAll = !(forCode || forTest);

async function main() {
  const { genSchema, createScemaData, saveSchemaFiles } = CodeGen({
    tsconfig: path.resolve(__dirname, '../tsconfig.json'),
    scriptName,
  });

  async function Schemas() {
    const schemasDir = path.resolve(__dirname, '../src/schemas');
    const modelSchemaFile = path.resolve(schemasDir, 'model.json');
    const sitemapSchemaFile = path.resolve(schemasDir, 'sitemap.json');
    const extractUrlSchemaFile = path.resolve(schemasDir, 'site-metadata.json');

    const hasModelSchema =
      !forAll && !forCode && fs.existsSync(modelSchemaFile);

    const modelSchema = (() => {
      if (hasModelSchema) return require(modelSchemaFile);

      const modelSchemas = ((...paths) =>
        paths.map((p) =>
          genSchema({
            path: path.resolve(__dirname, '../src/types/model', p),
          })
        ))('primitives/primitives.ts', 'base.ts', 'pagination.ts');

      let definitions = {};
      modelSchemas.forEach(
        (m) => (definitions = { ...definitions, ...m.definitions })
      );

      // From https://github.com/uuidjs/uuid/blob/main/src/regex.js
      definitions.ID = {
        ...definitions.ID,
        pattern:
          '^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$',
      };

      return { ...modelSchemas[0], definitions };
    })();

    const sitemapSchema =
      (forAll || forCode) &&
      genSchema({
        path: path.resolve(__dirname, '../src/types/sitemap.ts'),
      });

    const extractUrlSchema =
      (forAll || forCode) &&
      genSchema({
        path: path.resolve(__dirname, '../src/types/site-metadata.ts'),
      });

    if (!fs.existsSync(schemasDir)) fs.mkdirSync(schemasDir);

    if (!hasModelSchema)
      await writeSourceFile(modelSchemaFile, JSON.stringify(modelSchema));

    if (forAll || forCode) {
      await writeSourceFile(sitemapSchemaFile, JSON.stringify(sitemapSchema));

      await writeSourceFile(
        extractUrlSchemaFile,
        JSON.stringify(extractUrlSchema)
      );
    }

    return modelSchema;
  }

  async function TestSchemas(modelSchema) {
    if (!forAll && !forTest) return;

    const testSchemasDir = path.resolve(__dirname, '../tests/testschemas');
    if (!fs.existsSync(testSchemasDir)) fs.mkdirSync(testSchemasDir);

    const fieldsInfos = require(path.resolve(
      __dirname,
      '../tests/testmodel/fieldsInfo.json'
    ));

    const sysData = {
      Privilege: {
        path: '../../testmodel/privileges.json',
      },
      Empire: {
        path: '../../testmodel/empires.json',
      },
    };

    const testmodelSchemaData = createScemaData({
      inputDir: path.resolve(__dirname, '../tests/testmodel'),
      schemaName: 'test',
      generics: true,
      parentSchemas: [
        {
          schema: modelSchema,
          name: 'SCHEMAS',
          alias: 'MODEL_CORE',
          resolve: 'model',
        },
      ],
      fieldsInfos,
    });

    function reExportHooks() {
      const impUtils = { import: { utils: 'extractGeneratedData' } };

      return { Empire: impUtils, Privilege: impUtils };
    }

    await saveSchemaFiles({
      paths: {
        modelCoreLib: '../../../src',
        modelSourceIndexFile: '../../testmodel',
        fieldsInfosFile: '../../testmodel/fieldsInfo.json',
      },
      outputDir: testSchemasDir,
      schemaName: 'test',
      sysData,
      reExportHooks,
      ...testmodelSchemaData,
    });
  }

  const modelSchema = await Schemas();

  await TestSchemas(modelSchema);

  console.log('\x1b[32mSuccess!\x1b[0m');
}

main();
