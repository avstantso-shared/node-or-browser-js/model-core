const path = require('path');
const rimraf = require('rimraf');

rimraf.sync(path.resolve(__dirname, '../src/schemas'));
rimraf.sync(path.resolve(__dirname, '../tests/testschemas'));
console.log('\x1b[32mSuccess!\x1b[0m');
