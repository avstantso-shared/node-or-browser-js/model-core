import { NamesTree } from '@avstantso/node-or-browser-js--utils';

const s = '';

export const FIELDS = NamesTree.Names({
  id: s,
  putdate: s,
  upddate: s,
  version: s,
  active: s,
  name: s,
  description: s,
  module: s,
  system: s,
  author_id: s,
  author: s,
  caption: s,

  folding: s,
  controls: s,
}).Name;
