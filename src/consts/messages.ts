export const accessDenied = 'Access denied';
export const versionConflict = 'Version conflict';
export const dataDoesNotMatchSchema = 'Data does not match schema';
export const dataNotSet = 'Data not set';
export const idNotSet = 'Id not set';
export const invalidUUIDFormatForId = 'Invalid UUID format for id';
export const dataTooLongForColumn = 'Data too long for column';
export const attemptDataConsistencyViolationPrevented =
  'Attempt data consistency violation prevented';
