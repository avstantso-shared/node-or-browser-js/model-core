import { SiteMetadata } from '../types/site-metadata';

const youtubeUrlRegExSrc = '(youtube\\.com)|(youtu\\.be)';

const youTubeVideoRegex =
  /(?:\/embed\/|\/watch\?v=|\/(?:embed\/|v\/|watch\?.*v=|youtu\.be\/|embed\/|v=))([^&?#]+)/;

/**
 * @summary Extract video id from `Youtube` url
 * @see https://dev.to/rahulj9a/how-to-build-simple-link-preview-without-any-library-in-js-2j84
 */
const extractYouTubeVideoId = (url: string) => {
  const match = url.match(youTubeVideoRegex);
  return match ? match[1] : '';
};

export namespace YouTube {
  export type Utils = {
    /**
     * @summary `Youtube` url regex source
     */
    urlRegExSrc: typeof youtubeUrlRegExSrc;

    /**
     * @summary Extract video id from `Youtube` url
     */
    extractVideoId: typeof extractYouTubeVideoId;

    /**
     * @summary Extract video data by `Youtube` url
     * @see https://dev.to/rahulj9a/how-to-build-simple-link-preview-without-any-library-in-js-2j84
     */
    videoData: (url: string) => SiteMetadata.Data.Video;
  };
}

export const YouTube: YouTube.Utils = {
  urlRegExSrc: youtubeUrlRegExSrc,
  extractVideoId: extractYouTubeVideoId,
  videoData: (url: string): SiteMetadata.Data.Video => {
    const videoId = extractYouTubeVideoId(url);
    if (!videoId) return;

    const videoThumbnail = `https://img.youtube.com/vi/${videoId}/maxresdefault.jpg`;
    return { videoId, videoThumbnail };
  },
};
