import { TS, JS } from '@avstantso/node-or-browser-js--utils';
import { ImposibleCaseError } from '@avstantso/node-or-browser-js--errors';

import { SiteMetadata } from '../types/site-metadata';

export type Data = SiteMetadata.Data;
export namespace Data {
  export type AllowMultiple = SiteMetadata.Data.AllowMultiple;

  export namespace AllowMultiple {
    export type Keys = keyof AllowMultiple;

    export type Unpacked = Record<Keys, string[]>;
  }

  export type Unpacked = Omit<Data, AllowMultiple.Keys> &
    AllowMultiple.Unpacked;

  export namespace Utils {
    export type Pack = {
      (metadata: SiteMetadata.Data): SiteMetadata.Data;
      Field<T extends Data[keyof Data]>(field: T, def?: T): T;
    };

    export type Unpack = {
      (metadata: SiteMetadata.Data): Unpacked;
      Field(field: string | string[]): string[];
    };

    export type Merge = {
      (
        ...metadata: [
          SiteMetadata.Data,
          SiteMetadata.Data,
          ...SiteMetadata.Data[]
        ]
      ): SiteMetadata.Data;
    };
  }

  export type Utils = {
    /**
     * @summary Merge multiple metadata to one new
     */
    merge: Utils.Merge;

    /**
     * @summary Remove empty fields, duplicates from arrays and extract single items from array
     * @description ⚠ mutate metadata
     */
    pack: Utils.Pack;

    /**
     * @summary Replace all `allow multiple` fields to arrays
     * @description ⚠ mutate metadata
     */
    unpack: Utils.Unpack;
  };
}

const Def = {
  allowMultiple: TS.Type.Definer<Data.AllowMultiple.Keys>(),
};

const allowMultiple = Def.allowMultiple.Arr(
  'title',
  'description',
  'images',
  'sitename',
  'favicon',
  'domain'
);

function packDataField<T extends Data[keyof Data]>(field: T, def?: T): T {
  if (Array.isArray(field)) {
    const a = [...new Set(field.pack())];
    switch (a.length) {
      case 0:
        return def;
      case 1:
        return a[0] as T;
      default:
        return a as T;
    }
  } else return field || def;
}

const packData: Data.Utils.Pack = (metadata) => {
  Object.entries(metadata).forEach(([key, value]) => {
    const v = packDataField(value);
    if (v) JS.set.raw(metadata, key, v);
    else delete metadata[key as keyof SiteMetadata.Data];
  });

  return metadata;
};
packData.Field = packDataField;

function unpackDataField(field: string | string[]): string[] {
  return !field ? [] : Array.isArray(field) ? field : [field];
}

const unpackData: Data.Utils.Unpack = (metadata) => {
  allowMultiple.forEach(
    (key) => (metadata[key] = unpackDataField(metadata[key]))
  );

  return metadata as Data.Unpacked;
};
unpackData.Field = unpackDataField;

const mergeData: Data.Utils.Merge = (...metadata) => {
  const packed = metadata.pack();
  if (!packed.length) return {};

  if (1 === packed.length) return packData({ ...packed[0] });

  const r: Data.Unpacked = unpackData({ ...packed[0] });

  for (let i = 1; i < packed.length; i++)
    Object.entries(packed[i]).forEach(([key, value]) => {
      if (!JS.is.string(key, allowMultiple)) JS.set.raw(r, key, value);
      else if (JS.is.string(value)) r[key].push(value);
      else if (Array.isArray(value)) r[key].push(...value);
      else throw new ImposibleCaseError();
    });

  return packData(r);
};

export const Data: Data.Utils = {
  /**
   * @summary Merge multiple metadata to one new
   */
  merge: mergeData,

  /**
   * @summary Remove empty fields, duplicates from arrays and extract single items from array
   * @description ⚠ mutate metadata
   */
  pack: packData,

  /**
   * @summary Replace all `allow multiple` fields to arrays
   * @description ⚠ mutate metadata
   */
  unpack: unpackData,
};
