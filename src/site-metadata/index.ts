import { JS } from '@avstantso/node-or-browser-js--utils';

import { SiteMetadata as Types } from '../types/site-metadata';

import { YouTube } from './youtube';
import { Data, type Data as DataTypes } from './data';

export namespace SiteMetadata {
  export namespace Data {
    /**
     * @summary Data extracted by url from `Youtube`
     * @see https://dev.to/rahulj9a/how-to-build-simple-link-preview-without-any-library-in-js-2j84
     */
    export type Video = Types.Data.Video;

    /**
     * @summary Data extracted by url. Allow multiple values in field
     * @see https://jsonlink.io/docs#extract_endpoint as template
     */
    export type AllowMultiple = Types.Data.AllowMultiple;

    export namespace AllowMultiple {
      export type Unpacked = DataTypes.AllowMultiple.Unpacked;
    }
  }

  /**
   * @summary Data extracted by url
   * @see https://jsonlink.io/docs#extract_endpoint as template
   */
  export type Data = Types.Data;

  /**
   * @summary Data extracted by url and execution `duration`
   * @see https://jsonlink.io/docs#extract_endpoint as template
   */
  export type Response = Types.Response;

  /**
   * @summary Data extract request
   */
  export type Request = Types.Request;

  /**
   * @summary Request options
   */
  export namespace Options {
    /**
     * @summary Request options defaults
     */
    export type Defaults = Types.Options.Defaults;

    /**
     * @summary Base request options
     */
    export type Base = Types.Options.Base;

    /**
     * @summary Request options headers
     */
    export type Headers = {
      'accept-language'?: string;
    };

    /**
     * @summary Request options with headers
     */
    export type WithHeaders = Types.Options & { headers?: Options.Headers };
  }

  export type Options = Types.Options;
}

function isOptions<T extends SiteMetadata.Options = SiteMetadata.Options>(
  candidate: unknown
): candidate is T {
  return (
    candidate &&
    JS.is.object<T>(candidate) &&
    (('noCache' in candidate && JS.is.boolean(candidate.noCache)) ||
      ('defaults' in candidate &&
        JS.is.object(candidate.defaults) &&
        ('changefreq' in candidate.defaults || null)))
  );
}

export const SiteMetadata = {
  Options: { is: isOptions },

  /**
   * @summary `Youtube` utils
   */
  YouTube,

  /**
   * @summary `Data` utils
   */
  Data,
};
