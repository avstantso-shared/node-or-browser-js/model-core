import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class ValidationError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, ValidationError.prototype);
  }

  static is(error: unknown): error is ValidationError {
    return JS.is.error(this, error);
  }
}
