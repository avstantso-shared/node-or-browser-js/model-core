import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class ModelCoreError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, ModelCoreError.prototype);
  }

  static is(error: unknown): error is ModelCoreError {
    return JS.is.error(this, error);
  }
}
