import { BaseError, Details } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

import { MESSAGES } from '../../consts';

import { ModelErrorKind } from './ModelErrorKind';

export class ModelMethodError extends BaseError {
  model: string;
  method: string;
  kind: ModelErrorKind;
  details: Details.Union;

  constructor(
    model: string | Function,
    method: string | Function,
    message: string,
    internal?: Error
  ) {
    super(message, internal);
    this.model = JS.is.function(model) ? model.name : model;
    this.method = JS.is.function(method) ? method.name : method;

    if (MESSAGES.versionConflict === message)
      this.kind = ModelErrorKind.VersionConflict;
    else if (MESSAGES.accessDenied === message)
      this.kind = ModelErrorKind.AccessDenied;
    else if (message.startsWith(MESSAGES.dataTooLongForColumn))
      this.kind = ModelErrorKind.DataTooLongForColumn;
    else if (MESSAGES.attemptDataConsistencyViolationPrevented === message)
      this.kind = ModelErrorKind.DataConsistencyViolation;
    else this.kind = undefined;

    Object.setPrototypeOf(this, ModelMethodError.prototype);
  }

  static is(error: unknown): error is ModelMethodError {
    return JS.is.error(this, error);
  }

  protected toLogDetails(): object {
    return {
      ...super.toLogDetails(),
      model: this.model,
      method: this.method,
      kind: this.kind,
      ...(this.details ? { details: this.details } : {}),
    };
  }
}
