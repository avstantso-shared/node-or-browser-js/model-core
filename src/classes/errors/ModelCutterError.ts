import { JS } from '@avstantso/node-or-browser-js--utils';
import { ErrorObject } from 'ajv';
import { ModelCoreError } from './ModelCoreError';

export class ModelCutterError extends ModelCoreError {
  data: any;
  errors: ErrorObject[];

  constructor(
    message: string,
    data: any,
    errors: ErrorObject[],
    internal?: Error
  ) {
    super(message, internal);
    this.data = data;
    this.errors = errors;

    Object.setPrototypeOf(this, ModelCutterError.prototype);
  }

  static is(error: unknown): error is ModelCutterError {
    return JS.is.error(this, error);
  }

  protected toLogDetails(): object {
    return {
      ...super.toLogDetails(),
      data: this.data,
      errors: this.errors,
    };
  }
}
