import { Enum } from '@avstantso/node-or-browser-js--utils';

import * as _ from './enum';

export namespace ModelErrorKind {
  // @ts-ignore fix CI TS2589: Type instantiation is excessively deep and possibly infinite
  export type TypeMap = Enum.TypeMap.Make<
    _.ModelErrorKind.Type,
    _.ModelErrorKind.Key.List
  >;
}

export type ModelErrorKind = Enum.Value<ModelErrorKind.TypeMap>;

export const ModelErrorKind = Enum(
  _.ModelErrorKind,
  'ModelErrorKind'
)<ModelErrorKind.TypeMap>();
