export enum ModelErrorKind {
  VersionConflict = 1,
  AccessDenied,
  DataTooLongForColumn,
  DataConsistencyViolation,
}

export namespace ModelErrorKind {
  export type Type = typeof ModelErrorKind;

  export namespace Key {
    export type List = [
      'VersionConflict',
      'AccessDenied',
      'DataTooLongForColumn',
      'DataConsistencyViolation'
    ];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
