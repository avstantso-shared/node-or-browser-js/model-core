import moment from 'moment';
import { Cases, JS, TS } from '@avstantso/node-or-browser-js--utils';

import { SiteMap as Types } from './types/sitemap';

/**
 * @see https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
 */
export namespace SiteMap {
  export namespace Url {
    export type Loc = Types.Url.Loc;
    export type Changefreq = Types.Url.Changefreq;
    export type Attrs = Types.Url.Attrs;

    /**
     * @summary According to schema https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
     */
    export type Native = Types.Url.Native;

    /**
     * @summary SiteMap extension for Secure routes
     */
    export type Security = Types.Url.Security;

    /**
     * @summary SiteMap extension for routes SEO
     */
    export namespace SEO {
      export type MetaProperty = Types.Url.SEO.MetaProperty;

      export namespace MetaProperties {
        export type Provider = Types.Url.SEO.MetaProperties.Provider;
      }

      export type MetaProperties = Types.Url.SEO.MetaProperties;

      export namespace Robots {
        export type MaxImagePreview = Types.Url.SEO.Robots.MaxImagePreview;
      }

      export type Robots = Types.Url.SEO.Robots;

      export type Rating = Types.Url.SEO.Rating;

      export type Metadata = Types.Url.SEO.Metadata;
    }

    /**
     * @summary SiteMap extension for routes SEO
     */
    export type SEO = Types.Url.SEO;

    export namespace Metadata {
      export type Provider = Types.Url.Metadata.Provider;
    }

    export type Metadata = Types.Url.Metadata;
  }

  export type Url = Types.Url;

  export type UrlSet = Types.UrlSet;

  export namespace ToXML {
    export type Options = {
      /**
       * @default 'https'
       */
      protocol?: string;

      /**
       * @default 'localhost'
       */
      domain?: string;
    };
  }

  export type ToXML = {
    (sitemap: SiteMap, options?: ToXML.Options): string;
    (sitemap: SiteMap, domain: string): string;
  };
}

export type SiteMap = Types;

const toXml: SiteMap.ToXML = (sitemap, param: any): string => {
  const { protocol, domain }: SiteMap.ToXML.Options = {
    protocol: 'https',
    domain: 'localhost',
    ...(JS.is.string(param) ? { domain: param } : param || {}),
  };

  function urlSet2Xml(urlset: SiteMap.UrlSet): string {
    return (
      `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\r\n` +
      urlset
        .filter((ulr) => !ulr.hidden && !ulr.private)
        .map(
          ({ loc, lastmod, changefreq, priority }) =>
            `  <url>\r\n` +
            `    <loc>${protocol}://${domain}${loc}</loc>\r\n` +
            (undefined !== lastmod
              ? `    <lastmod>${moment(lastmod).format()}</lastmod>\r\n`
              : '') +
            (undefined !== changefreq
              ? `    <changefreq>${changefreq}</changefreq>\r\n`
              : '') +
            (undefined !== priority
              ? `    <priority>${priority}</priority>\r\n`
              : '') +
            `  </url>\r\n`
        )
        .join('') +
      `</urlset>`
    );
  }

  const header = `<?xml version="1.0" encoding="UTF-8"?>\r\n`;

  if (0 === sitemap.length) return header + `<urlset>\r\n</urlset>`;

  if (!Array.isArray(sitemap[0]))
    return header + urlSet2Xml(sitemap as SiteMap.UrlSet);

  return header + (sitemap as SiteMap.UrlSet[]).map(urlSet2Xml).join('\r\n');
};

/**
 * @summary Used with \@avstantso/node-js--rollup-date-from-git
 */
export function lastmodFromGit(...relPaths: string[]): Date {
  return undefined;
}

/**
 * @summary Used with \@avstantso/node-js--rollup-date-from-git
 */
export function creationFromGit(...relPaths: string[]): Date {
  return undefined;
}

export function fuzzySearch(sitemap: SiteMap, url: string): SiteMap.Url {
  const urlParts = url.split('/');

  function match(parts: string[]): boolean {
    for (let i = 0; i < Math.min(urlParts.length, parts.length); i++) {
      const u = urlParts[i];
      const p = parts[i];
      if (u !== p && !p.startsWith(':')) return false;
    }

    return true;
  }

  function search(urlset: SiteMap.UrlSet): SiteMap.Url {
    let r: SiteMap.Url;

    for (let i = 0; i < urlset.length; i++) {
      const parts = urlset[i].loc.split('/');
      if (parts.length > urlParts.length || !match(parts)) continue;

      if (!r || r.loc.length < urlset[i].loc.length) r = urlset[i];
    }

    return r;
  }

  if (0 === sitemap.length) return undefined;

  if (!Array.isArray(sitemap[0])) return search(sitemap as SiteMap.UrlSet);

  for (let i = 0; i < sitemap.length; i++) {
    let r = search(sitemap[i] as SiteMap.UrlSet);
    if (r) return r;
  }
}

export function og(
  property: string,
  content: SiteMap.Url.SEO.MetaProperty['content']
): SiteMap.Url.SEO.MetaProperty {
  return { property: `og:${property}`, content };
}

function ogComplete(
  properties: SiteMap.Url.SEO.MetaProperties,
  seoItem: SiteMap.Url.SEO
): SiteMap.Url.SEO.MetaProperties {
  const { robots, ...rest } = seoItem;

  const r: SiteMap.Url.SEO.MetaProperties = properties ? [...properties] : [];

  const ogType = r.find(({ property }) => property === `og:type`);
  if (!ogType) r.push(og('type', 'website'));

  Object.keys(rest).forEach((name) => {
    const content = rest[name as keyof typeof rest];

    if (!r.find(({ property }) => property === `og:${name}`))
      r.push(og(name, content));
  });

  return r;
}

function isSeoRobots(param: unknown): param is SiteMap.Url.SEO.Robots {
  return (
    null !== param &&
    JS.is.object(param) &&
    ('index' in param ||
      'noindex' in param ||
      'follow' in param ||
      'nofollow' in param ||
      'none' in param ||
      'all' in param ||
      'noimageindex' in param ||
      'noarchive' in param ||
      'nocache' in param ||
      'nosnippet' in param ||
      'nositelinkssearchbox' in param ||
      'nopagereadaloud' in param ||
      'notranslate' in param ||
      'maxSnippet' in param ||
      'maxVideoPreview' in param ||
      'maxImagePreview' in param ||
      'rating' in param ||
      'unavailable_after' in param ||
      'noyaca' in param ||
      'noydir' in param)
  );
}

function seoRobots2MetaStr(robots: SiteMap.Url.SEO.Robots): string {
  const r: string[] = [];
  Object.keys(robots).forEach((key) => {
    const value = robots[key as keyof SiteMap.Url.SEO.Robots];
    if (undefined === value || null === value || false === value) return;

    const name = Cases.kebab(key);

    r.push(`${name}${JS.is.boolean(value) ? '' : `:${value}`}`);
  });

  return r.join(', ');
}

const ChangefreqMomentRelations = {
  hourly: 'hour',
  daily: 'day',
  weekly: 'week',
  monthly: 'month',
  yearly: 'year',
};

function ChangefreqAsMSec(changefreq: SiteMap.Url.Changefreq): number {
  if (!changefreq) return null;

  switch (changefreq) {
    case 'always':
      return 0;
    case 'never':
      return Infinity;
  }

  const uot: moment.unitOfTime.Base = JS.get.raw(
    ChangefreqMomentRelations,
    changefreq
  );

  return uot ? moment(new Date(0)).add(1, uot).valueOf() : NaN;
}

export const SiteMap = {
  /**
   * @summary Convert to xml
   */
  toXml,

  /**
   * @summary Fuzzy search by near loc
   */
  fuzzySearch,

  /**
   * @summary Create og meta property
   */
  og,

  /**
   * @summary Add og props if not exists by seo props
   */
  ogComplete,

  Url: {
    Changefreq: {
      always: 'always' as SiteMap.Url.Changefreq,
      hourly: 'hourly' as SiteMap.Url.Changefreq,
      daily: 'daily' as SiteMap.Url.Changefreq,
      weekly: 'weekly' as SiteMap.Url.Changefreq,
      monthly: 'monthly' as SiteMap.Url.Changefreq,
      yearly: 'yearly' as SiteMap.Url.Changefreq,
      never: 'never' as SiteMap.Url.Changefreq,

      /**
       * @summary As milliseconds. How long wait changes?
       * @return Milliseconds. `null` for empty values, `NaN` for bad values, `Infinity` for `never`
       */
      asMilliseconds: ChangefreqAsMSec,
    },
    Attrs: TS.Type.Definer<keyof SiteMap.Url.Attrs>().Arr(
      'lastmod',
      'changefreq',
      'priority'
    ),
    SEO: {
      Robots: {
        /**
         * @summary Object type is Robots SEO object
         */
        is: isSeoRobots,

        /**
         * @summary Robots SEO object to mrta tag string
         */
        toMetaStr: seoRobots2MetaStr,
      },
    },
  },
};
