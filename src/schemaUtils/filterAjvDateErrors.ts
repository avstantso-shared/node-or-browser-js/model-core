import type { ErrorObject } from 'ajv';
import { resovleSchemaRef } from './resovleSchemaRef';

export function filterAjvDateErrors<TIn = any>(entity: TIn) {
  return (error: ErrorObject) => {
    const { keyword, instancePath } = error;
    return !(
      keyword === 'type' &&
      resovleSchemaRef(entity, instancePath) instanceof Date
    );
  };
}
