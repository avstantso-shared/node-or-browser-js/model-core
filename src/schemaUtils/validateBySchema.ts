import type { JSONSchema7 } from 'json-schema';

import { Clone, ValueWrap } from '@avstantso/node-or-browser-js--utils';

import { Ajv } from '../ajv';
import { filterAjvDateErrors } from './filterAjvDateErrors';

export function validateBySchema<TData = any>(
  data: TData,
  schema: JSONSchema7
) {
  const ajv = Ajv();

  const validate = ajv.compile<TData>(schema);
  const reference: TData = Clone.Deep(data, ValueWrap.cloningResolve);

  const isValid = validate(reference);
  if (isValid) return { isValid };

  const errors = validate.errors.filter(filterAjvDateErrors(data));

  return {
    isValid: isValid || !errors.length,
    ...(errors.length ? { errors } : {}),
  };
}
