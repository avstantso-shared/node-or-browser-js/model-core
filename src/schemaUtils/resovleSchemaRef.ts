import { Generics } from '@avstantso/node-or-browser-js--utils';

export function resovleSchemaRef<TOut = any, TIn = any>(
  entity: TIn,
  ref: string
): TOut {
  if (!ref) return Generics.Cast.To(entity);

  const names = ref.split('/').slice(1);

  const recursion = (aData: any, index: number): any => {
    if (index >= names.length) return aData;

    const name = names[index];
    if (Array.isArray(aData))
      return recursion(aData[Number.parseInt(name)], index + 1);

    return recursion(aData[name as keyof any], index + 1);
  };

  return recursion(entity, 0);
}
