import type { JSONSchema7 } from 'json-schema';

export function subSchema(
  subSchema: JSONSchema7,
  ...schemas: JSONSchema7[]
): JSONSchema7 {
  let definitions: object;
  schemas.forEach(
    (schema) =>
      (definitions = { ...(definitions || {}), ...schema.definitions })
  );

  return {
    ...subSchema,
    ...(definitions ? { definitions } : {}),
  } as JSONSchema7;
}
