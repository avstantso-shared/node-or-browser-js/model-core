export type SchemaProperties = {
  properties?: unknown;
} & { [name: string]: unknown };

export function fieldsBySchema<T extends SchemaProperties>(
  schema: T
): Record<keyof T['properties'], string> {
  const r: any = {};

  if (schema?.properties)
    Object.keys(schema.properties).forEach((key) => {
      r[key] = key;
    });

  return r;
}
