import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { ValidationError } from './classes';

export namespace FieldValidator {
  export type Checker<T> = (field: T) => boolean;
}

export function FieldValidator<T = any>(
  checker: RegExp | FieldValidator.Checker<T>,
  errorMessage: string
) {
  return (field: T): T => {
    if (
      JS.is.function<FieldValidator.Checker<T>>(checker)
        ? !checker(field)
        : !checker.test(Generics.Cast.To(field))
    )
      throw new ValidationError(errorMessage);

    return field;
  };
}
