import type { JSONSchema7 } from 'json-schema';
import { JS } from '@avstantso/node-or-browser-js--utils';
import { ModelCoreError } from '../classes';

import type { Model } from '../types';

export namespace Declaration {
  export type Schema = JSONSchema7 &
    Partial<Record<Model.StructureKeys, JSONSchema7>>;

  export interface Fields {
    [key: string]: string;
  }

  export interface Peculiarities {
    idCanChangedAfterUpdate?: boolean;
  }

  export namespace Types {
    export type Type<TCutter = unknown> = {
      Schema: JSONSchema7;
      Fields: Fields;
    } & (TCutter extends Function ? { Cutter: TCutter } : {});
    export type Master<TModel extends Model.Structure> = Type;

    export type Minimal<TModel extends Model.Structure> = Type<
      Model.Cutter.ToMinimal<TModel>
    >;
    export type Insert<TModel extends Model.Structure> = Type<
      Model.Cutter.ToInsert<TModel>
    >;
    export type InsertSecrets<TModel extends Model.Structure> = Type;
    export type InsertTree<TModel extends Model.Structure> = Type<
      Model.Cutter.ToInsertTree<TModel>
    >;
    export type InsertDetail<TModel extends Model.Structure> = Type;
    export type Update<TModel extends Model.Structure> = Type<
      Model.Cutter.ToUpdate<TModel>
    >;
    export type UpdateTree<TModel extends Model.Structure> = Type<
      Model.Cutter.ToUpdateTree<TModel>
    >;
    export type UpdateDetail<TModel extends Model.Structure> = Type;

    export type Select<TModel extends Model.Structure> = Type;
    export type Condition<TModel extends Model.Structure> = Type;

    export type Union<TModel extends Model.Structure> =
      | Master<TModel>
      | Minimal<TModel>
      | Insert<TModel>
      | InsertSecrets<TModel>
      | InsertTree<TModel>
      | InsertDetail<TModel>
      | Update<TModel>
      | UpdateTree<TModel>
      | UpdateDetail<TModel>
      | Select<TModel>
      | Condition<TModel>;
  }

  export interface Types<TModel extends Model.Structure> {
    Master?: Types.Master<TModel>;

    Minimal?: Types.Minimal<TModel>;
    Insert?: Types.Insert<TModel>;
    InsertSecrets?: Types.InsertSecrets<TModel>;
    InsertTree?: Types.InsertTree<TModel>;
    InsertDetail?: Types.InsertDetail<TModel>;

    Update?: Types.Update<TModel>;
    UpdateTree?: Types.UpdateTree<TModel>;
    UpdateDetail?: Types.UpdateDetail<TModel>;

    Select: Types.Select<TModel>;
    Condition: Types.Condition<TModel>;
  }

  export interface Base<TModel = unknown> {
    Model: TModel;
    name: string;
    Schema: Declaration.Schema;
    Fields: Declaration.Fields;
    FieldsInfo?: Model.FieldsInfo;
    peculiarities?: Peculiarities;
  }
}

export type Declaration<TModel extends Model.Structure> =
  Declaration.Types<TModel> & Declaration.Base<TModel>;

export const Declaration = {
  /**
   * @summary Typecheck for a declaration and check it fields
   */
  Check: <
    TModel extends Model.Structure,
    TDeclaration extends Declaration<TModel>
  >(
    declaration: TDeclaration
  ) => {
    const { name, Schema, Fields } = declaration;

    Object.keys({ name, Schema, Fields }).forEach((key) => {
      if (!declaration[key as keyof TDeclaration])
        throw new ModelCoreError(`Declaration "${key}" is empty`);
    });

    Object.keys({ Schema, Fields }).forEach((key) => {
      if (!JS.is.object(declaration[key as keyof TDeclaration]))
        throw new ModelCoreError(
          `Declaration "${key}" must be a "${JS.object}"`
        );
    });
  },
  Type: {
    /**
     * @summary Typecheck for a declaration type
     */
    Check: <
      TModel extends Model.Structure,
      TType extends Declaration.Types.Union<TModel>
    >(
      type: TType
    ) => {
      if (!type) throw new ModelCoreError(`Declaration.Type is empty`);
    },
  },
};
