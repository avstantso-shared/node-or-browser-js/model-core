/**
 * @summary Model structure types enum
 */
export enum ModelTypes {
  Master = 1,

  Minimal,

  Insert,
  InsertSecrets,
  InsertTree,
  InsertDetail,

  Update,
  UpdateTree,
  UpdateDetail,

  Select,

  Condition,
}

export namespace ModelTypes {
  export type Type = typeof ModelTypes;

  export namespace Key {
    export type List = [
      'Master',
      'Minimal',
      'Insert',
      'InsertSecrets',
      'InsertTree',
      'InsertDetail',
      'Update',
      'UpdateTree',
      'UpdateDetail',
      'Select',
      'Condition'
    ];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
