import { Enum } from '@avstantso/node-or-browser-js--utils';

import * as _ from './enum';

/**
 * @summary Model structure types enum
 */
export namespace Types {
  export type TypeMap = Enum.TypeMap.Make<
    _.ModelTypes.Type,
    _.ModelTypes.Key.List
  >;
}

export type Types = Enum.Value<Types.TypeMap>;

export const Types = Enum(_.ModelTypes, 'Model.Types')<Types.TypeMap>();
