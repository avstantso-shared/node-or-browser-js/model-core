import type { JSONSchema7 } from 'json-schema';
import { Clone, ValueWrap } from '@avstantso/node-or-browser-js--utils';

import { Ajv, ErrorObject } from './ajv';
import { resovleSchemaRef, filterAjvDateErrors } from './schemaUtils';
import { ModelCutterError } from './classes';

const additionalProperties = 'additionalProperties';

export namespace CutBySchema {
  export interface Debugger {
    noCuttingRequired(): void;
    forCutting(errors: ErrorObject[]): void;
  }

  export interface Options {
    debug?: Debugger;
    ignoreError?: Parameters<ErrorObject[]['filter']>[0];
  }
}

/**
 * @summary Cut additional properties by schema.
 * Ignore Date instead of string.
 * Throw any other ajv errors
 */
export function cutBySchema<TFrom = any, TTo = any>(
  data: TFrom,
  schemaTo: JSONSchema7,
  options?: CutBySchema.Options
): TTo {
  const { debug, ignoreError } = options || {};

  const ajv = Ajv();

  const validate = ajv.compile<TTo>(schemaTo);

  const reference: TTo = Clone.Deep(data, ValueWrap.cloningResolve);
  const isValid = validate(reference);

  const result: TTo = Clone.Deep(data, ValueWrap.cloningSame);

  if (isValid) {
    debug && debug.noCuttingRequired();
    return result;
  }

  const errors = validate.errors.filter(filterAjvDateErrors(result));
  debug && debug.forCutting(errors);

  const noAPErrors = (
    ignoreError ? errors.filter((...params) => !ignoreError(...params)) : errors
  ).filter(({ keyword }) => keyword !== additionalProperties);

  if (noAPErrors.length)
    throw new ModelCutterError(
      `Has ${noAPErrors.length} schema errors. See details`,
      data,
      noAPErrors
    );

  errors.forEach((error) => {
    const sd = resovleSchemaRef(result, error.instancePath);
    delete sd[error.params.additionalProperty];
  });

  return result;
}
