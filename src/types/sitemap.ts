import type { LocalString } from './localization';

/**
 * @see https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
 */
export namespace SiteMap {
  export namespace Url {
    export type Loc = {
      loc: string;
    };

    export type Changefreq =
      | 'always'
      | 'hourly'
      | 'daily'
      | 'weekly'
      | 'monthly'
      | 'yearly'
      | 'never';

    export type Attrs = {
      lastmod?: Date;
      changefreq?: Changefreq;
      priority?: number;
    };

    /**
     * @summary According to schema https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
     */
    export type Native = Loc & Attrs;

    /**
     * @summary SiteMap extension for Secure routes
     */
    export type Security = {
      hidden?: boolean;
      private?: boolean;
    };

    /**
     * @summary SiteMap extension for routes SEO
     */
    export namespace SEO {
      export type MetaProperty = {
        property: string;
        content: string | LocalString | Date;
      };

      export namespace MetaProperties {
        export type Provider = { properties?: MetaProperties };
      }

      export type MetaProperties = MetaProperty[];

      // Source
      // https://yoast.com/robots-meta-tags/
      export namespace Robots {
        export type MaxImagePreview = 'none' | 'standard' | 'large';
      }

      export type Robots = {
        index?: boolean;
        noindex?: boolean;
        follow?: boolean;
        nofollow?: boolean;
        none?: boolean;
        all?: boolean;
        noimageindex?: boolean;
        noarchive?: boolean;
        nocache?: boolean;
        nosnippet?: boolean;
        nositelinkssearchbox?: boolean;
        nopagereadaloud?: boolean;
        notranslate?: boolean;
        maxSnippet?: number;
        maxVideoPreview?: number;
        maxImagePreview?: Robots.MaxImagePreview;
        rating?: string;
        unavailable_after?: Date;
        noyaca?: boolean;
        /**
         * @deprecated
         */
        noydir?: boolean;
      };

      export type Rating =
        | 'general'
        | 'mature'
        | 'restricted'
        | 'adult'
        | '14 years'
        | 'safe for kids';

      export type Metadata = SEO & MetaProperties.Provider;
    }

    /**
     * @summary SiteMap extension for routes SEO
     */
    export type SEO = {
      title?: string | LocalString;
      description?: string | LocalString;
      keywords?: string | LocalString;
      author?: string | LocalString;
      robots?: SEO.Robots;
      language?: string | LocalString;
      copyright?: string | LocalString;
      generator?: string | LocalString;
      news_keywords?: string | LocalString;
      rating?: SEO.Rating;
    };

    export namespace Metadata {
      export type Provider = { sitemapMeta?: Metadata };
    }

    export type Metadata = Attrs & Security & SEO.Metadata;
  }

  export type Url = Url.Loc & Url.Metadata;

  export type UrlSet = Url[];
}

export type SiteMap = SiteMap.UrlSet | SiteMap.UrlSet[];
