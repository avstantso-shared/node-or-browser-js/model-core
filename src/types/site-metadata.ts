import { SiteMap } from './sitemap';

export namespace SiteMetadata {
  export namespace Data {
    /**
     * @summary Data extracted by url from `Youtube`
     * @see https://dev.to/rahulj9a/how-to-build-simple-link-preview-without-any-library-in-js-2j84
     */
    export type Video = {
      videoId: string;
      videoThumbnail: string;
    };

    /**
     * @summary Data extracted by url. Allow multiple values in field
     * @see https://jsonlink.io/docs#extract_endpoint as template
     */
    export type AllowMultiple = Record<
      'title' | 'description' | 'images' | 'sitename' | 'favicon' | 'domain',
      string | string[]
    >;
  }

  /**
   * @summary Data extracted by url
   * @see https://jsonlink.io/docs#extract_endpoint as template
   */
  export type Data = SiteMap.Url.Attrs &
    Partial<Data.Video> &
    Partial<Data.AllowMultiple> & {
      url?: string;
      source?: string;
    };

  /**
   * @summary Data extracted by url and execution `duration`
   * @see https://jsonlink.io/docs#extract_endpoint as template
   */
  export type Response = Data & {
    duration: number;
  };

  /**
   * @summary Data extract request
   */
  export type Request = {
    url: string;
    options?: Options;
  };

  /**
   * @summary Request options
   */
  export namespace Options {
    /**
     * @summary Default values ​​if not received previously
     */
    export type Defaults = {
      changefreq?: SiteMap.Url.Changefreq;
    };

    /**
     * @summary Base request options
     */
    export type Base = {
      /**
       * @summary Default values ​​if not received previously
       */
      defaults?: Defaults;

      /**
       * @summary Ignore cache
       */
      noCache?: boolean;

      /**
       * @summary Use old cache if new data reciving fails
       */
      oldCacheOnFail?: boolean;

      /**
       * @summary If none images in `HEAD`, limit for search images count in `BODY`
       */
      bodyImgLimit?: number;

      /**
       * @summary Don't use robots and sitemap query
       */
      noSitemap?: boolean;

      /**
       * @summary Ignore `HEAD` subtags except `og` meta tags. Will ignored `TITLE`, `DESCRIPTION` and etc.
       */
      ogHeadOnly?: boolean;

      /**
       * @summary Request timeout
       */
      timeout?: number;

      /**
       * @summary Expected charset
       * @default `utf-8`
       */
      charset?: string;
    };
  }

  export type Options = Options.Base;
}
