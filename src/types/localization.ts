/**
 * @description Need be sync with `src/localization.ts`
 */
export interface LocalString {
  en: string;
  ru: string;
}
