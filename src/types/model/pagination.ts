import { Data } from '../data';

export namespace Pagination {
  export namespace Result {
    /**
     * @summary Total records count
     */
    export type Total = {
      total: number;
    };

    /**
     * @summary Pagination result object
     */
    export type AsObject<TModel extends { Select: unknown }> = Total &
      Data<TModel['Select'][]>;

    /**
     * @summary Pagination result convert function
     */
    export type Convert<TModel extends { Select: unknown }, TItem> = (
      from: Pagination.Result<TModel>
    ) => Pagination.Result<{ Select: TItem }>;
  }

  /**
   * @summary Pagination result: total count, page items
   */
  export type Result<TModel extends { Select: unknown }> = [
    number,
    TModel['Select'][]
  ];
}

/**
 * @summary Pagination for client and server
 */
export interface Pagination {
  /**
   * Page number
   */
  readonly num: number;

  /**
   * Page items limit
   */
  readonly size: number;
}
