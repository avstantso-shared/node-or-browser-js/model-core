export type ID = string;
export namespace ID {
  export type Provider = {
    id: ID;
  };
}
export type IDed = ID.Provider;

export namespace PutDate {
  export type Provider = {
    putdate: Date;
  };
}

export namespace UpdDate {
  export type Provider = {
    upddate: Date;
  };
}

export type Dates = PutDate.Provider & UpdDate.Provider;

export namespace Expires {
  export type Provider = {
    expires: Date;
  };
}
export type Expirable = Expires.Provider;

export namespace Active {
  export type Provider = {
    /**
     * @default true
     */
    active?: boolean;
  };
}
export type Activable = Active.Provider;

export type Name = string;
export namespace Name {
  export type Provider = { name: Name };
}
export type Named = Name.Provider;

export type Description = string;
export namespace Description {
  export type Provider = { description?: Description };
}
export type Described = Description.Provider;

export namespace Module {
  export type Provider = { module: string };
}
export type Moduled = Module.Provider;

export namespace System {
  export type Provider = { system: boolean };
}
export type Systemic = System.Provider;

export namespace Version {
  export type Provider = { version: number };
}
export type Versioned = Version.Provider;

export namespace Author {
  export namespace ID {
    export type Provider = { author_id: ID };
  }

  export namespace Login {
    export type Provider = {
      author: Login;
    };
  }
}

export namespace Authored {
  export type ID = Author.ID.Provider;
  export type Login = Author.Login.Provider;
}

export type Login = string;
export namespace Login {
  export type Provider = { login: Login };
}
export type Logined = Login.Provider;
