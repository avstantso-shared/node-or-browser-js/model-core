import type { Structure } from './structure';

export type Func<TFrom, TTo> = (data: TFrom) => TTo;
export type Maximal<T extends Structure> = T['UpdateTree'] | T['Select'];
export type ToMinimal<T extends Structure> = Func<
  T['Minimal'] | T['Select'],
  T['Minimal']
>;
export type ToInsert<T extends Structure> = Func<
  T['Insert'] | T['InsertTree'] | T['Update'] | Maximal<T>,
  T['Insert']
>;
export type ToUpdate<T extends Structure> = Func<
  T['Update'] | Maximal<T>,
  T['Update']
>;
export type ToInsertTree<T extends Structure> = Func<
  T['InsertTree'] | Maximal<T>,
  T['InsertTree']
>;
export type ToUpdateTree<T extends Structure> = Func<
  Maximal<T>,
  T['UpdateTree']
>;

export type Readonly<T extends Structure> = {
  toMinimal: ToMinimal<T>;
};

export type Simple<T extends Structure> = Readonly<T> & {
  toInsert: ToInsert<T>;
  toUpdate: ToUpdate<T>;
};

export type Hierarchical<T extends Structure> = Simple<T> & {
  toInsertTree: ToInsertTree<T>;
  toUpdateTree: ToUpdateTree<T>;
};

export type Union<T extends Structure> =
  | Readonly<T>
  | Simple<T>
  | Hierarchical<T>;
