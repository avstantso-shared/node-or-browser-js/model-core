export type FieldInfo = {
  length?: { max?: number; min?: number };
  pattern?: string;
};

//A.V.Stantso:
//  = NodeJS.Dict<FieldInfo>
//  has problems with "ts-json-schema-generator"
export type FieldsInfo = {
  [key: string]: FieldInfo;
};
