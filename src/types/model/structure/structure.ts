// A.V.Stantso:
//  !!! Warning !!!
//  Structure namespace not supported by TS Schema generator

import type { Minimal, Insert, Update, Select } from '../base';

export type StructureKeys = keyof Omit<
  Structure,
  'Need extends Model.Structure'
>;

export interface Structure {
  /**
   * @summary master entity fields
   * @requires is detail
   */
  Master?: unknown;

  /**
   * @summary minimal entity unique key fields set
   * @requires unique key exists
   */
  Minimal?: Minimal;

  /**
   * @summary entity fields set for insert
   * @requires is not readonly type
   */
  Insert?: Insert;

  /**
   * @summary entity extended secret fields set for insert
   * @requires has secrets
   */
  InsertSecrets?: unknown;

  /**
   * @summary entity fields set for insert hierarchical data with children nodes
   * @requires is hierarchical type
   * @returns Insert + _All_children[].InsertDetail
   */
  InsertTree?: Insert;

  /**
   * @summary entity fields set for insert without master (with hierarchical data, if it`s exists)
   * @requires is detail
   * @returns InsertTree - Master
   */
  InsertDetail?: Insert; // = (Insert - Master) + Partial<Master> + <All children>[].InsertDetail

  /**
   * @summary entity fields set for update
   * @requires is not readonly type
   * @returns IUpdate + Insert
   */
  Update?: Update;

  /**
   * @summary entity fields set for update hierarchical data with children nodes
   * @requires is hierarchical type
   * @returns Update + _All_children[].UpdateDetail
   */
  UpdateTree?: Update;

  /**
   * @summary entity fields set for update without master (with hierarchical data, if it`s exists)
   * @requires is detail
   * @returns UpdateTree - Master
   */
  UpdateDetail?: Update; // = (Update - Master) + Partial<Master> + <All children>[].UpdateDetail

  /**
   * @summary entity fields set for select
   * @returns Minimal + Insert + _Select_Only_Fields
   */
  Select: Select;

  /**
   * @summary entity fields set for filtering select
   * @returns Omit(Select, keyof ISelect)
   */
  Condition: {};

  /**
   * @summary guardian for no inheritance errors
   */
  'Need extends Model.Structure': 'Guardian';
}
