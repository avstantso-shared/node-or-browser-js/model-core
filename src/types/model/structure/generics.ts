import type * as Base from '../base';
import type { StructureMeta } from './meta';

// A.V.Stantso:
//  type checking broken for this generics in derived libraries :(
//  need manually reproduce Structure instead of apply StructureFrom<>

export type Master<T extends StructureMeta> = Omit<T['master'], 'entity'>;
export type Minimal<T extends StructureMeta> = Base.Minimal &
  Master<T> &
  T['minimal'];
export type Insert<T extends StructureMeta> = Base.Insert &
  Master<T> &
  T['insert'];
export type InsertSecrets<T extends StructureMeta> = T['insertSecrets'];
export type InsertTree<T extends StructureMeta> = Base.Insert &
  Master<T> &
  T['insert'] &
  T['insertTree'];
export type InsertDetail<T extends StructureMeta> = Base.Insert &
  Partial<Master<T>> &
  T['insert'] &
  T['insertTree'];
export type Update<T extends StructureMeta> = Base.Update & Insert<T>;
export type UpdateTree<T extends StructureMeta> = Update<T> & T['updateTree'];
export type UpdateDetail<T extends StructureMeta> = Base.Update &
  Partial<Master<T>> &
  T['insert'] &
  T['updateTree'];
export type Select<T extends StructureMeta> = Base.Select &
  Minimal<T> &
  Update<T> &
  T['select'];
export type Condition<T extends StructureMeta> = Partial<
  Minimal<T> & T['insert'] & T['select']
>;
