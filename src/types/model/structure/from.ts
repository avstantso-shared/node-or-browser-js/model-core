import type { StructureMeta } from './meta';
import type { Structure } from './structure';
import type {
  Master,
  Minimal,
  Insert,
  InsertSecrets,
  InsertTree,
  InsertDetail,
  Update,
  UpdateTree,
  UpdateDetail,
  Select,
  Condition,
} from './generics';

export interface StructureFrom<T extends StructureMeta> extends Structure {
  Master?: Master<T>;
  Minimal?: Minimal<T>;
  Insert?: Insert<T>;
  InsertSecrets?: InsertSecrets<T>;
  InsertTree?: InsertTree<T>;
  InsertDetail?: InsertDetail<T>;
  Update?: Update<T>;
  UpdateTree?: UpdateTree<T>;
  UpdateDetail?: UpdateDetail<T>;
  Select: Select<T>;
  Condition: Condition<T>;
}
