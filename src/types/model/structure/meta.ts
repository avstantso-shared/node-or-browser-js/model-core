// A.V.Stantso:
//  !!! Warning !!!
//  Structure namespace not supported by TS Schema generator

export type StructureMetaKeys = keyof Omit<
  StructureMeta,
  'Need extends Model.MetaStructure'
>;

export interface StructureMetaMaster {
  /**
   * @summary master type
   */
  entity: unknown;

  // master link fields in derived type...
}

export interface StructureMeta {
  /**
   * @summary master entity link
   * @requires is detail
   */
  master?: StructureMetaMaster;

  /**
   * @summary minimal entity unique key fields set
   * @requires unique key exists
   */
  minimal?: unknown;

  /**
   * @summary entity fields set for insert and update (without: IUpdate)
   * @requires is not readonly type
   */
  insert?: unknown;

  /**
   * @summary entity extended secret fields set for insert
   * @requires has secrets
   */
  insertSecrets?: unknown;

  /**
   * @summary entity fields set for insert hierarchical data with children nodes
   * @requires is hierarchical type
   */
  insertTree?: unknown;

  /**
   * @summary entity fields set for update hierarchical data with children nodes
   * @requires is hierarchical type
   */
  updateTree?: unknown;

  /**
   * @summary entity fields set for select (without: ISelect & 'insert')
   * @requires exists additional fields extends 'minimal' and 'insert'
   */
  select?: unknown;

  /**
   * @summary guardian for no inheritance errors
   */
  'Need extends Model.MetaStructure': 'Guardian';
}
