import type { Dates, IDed, Versioned } from './primitives';
import type { Pagination } from './pagination';

/**
 * @summary minimal entity fields set
 */
export type Minimal = {};

/**
 * @summary entity fields set for insert
 */
export type Insert = Minimal;

/**
 * @summary entity fields set for update
 */
export type Update = Insert & IDed & Versioned;

/**
 * @summary entity fields set for select
 */
export type Select = Update & Dates;

export namespace Select {
  export namespace Options {
    export type Serialized = {
      pagination?: Record<keyof Pagination, string>;
      search?: string;
    };
  }

  /**
   * @summary selection options for client and server
   */
  export type Options<
    TModel extends { Condition: unknown },
    TPagination extends Pagination = Pagination
  > = {
    pagination?: TPagination;
    search?: TModel['Condition'];
  };
}
