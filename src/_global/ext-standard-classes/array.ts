namespace AVStantso.Array {
  export namespace Ex {
    // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
    export namespace By {
      /**
       * @summary This algorithm uses in implementation
       */
      export namespace Algorithm {
        export type Predicated = 'P';
        export type Sort = 'S';

        export type Map = {
          [K in Algorithm]: <
            M extends keyof Methods.Key.Map.StdW,
            R = unknown,
            F extends (...args: any[]) => R = (...args: any[]) => R
          >(
            method?: M,
            transform?: (result: ReturnType<globalThis.Array<unknown>[M]>) => R
          ) => F;
        };
      }

      export type Algorithm = Algorithm.Predicated | Algorithm.Sort;

      export namespace Methods {
        //#region Template
        export type Template<
          W extends boolean,
          Find = unknown,
          FindIndex = unknown,
          Some = unknown,
          Every = unknown,
          Filter = unknown,
          Sort = unknown,
          Extract = unknown,
          Count = unknown
        > = {
          /**
           * @summary Find item by value or value field
           * @param value Value as find criteria
           * @param options Find options
           * @returns Item
           */
          find: Find;

          /**
           * @summary Find item index by value or value field
           * @param value Value as find criteria
           * @param options Find options
           * @returns Item index
           */
          findIndex: FindIndex;

          /**
           * @summary Check exists one or more items by value or value field
           * @param value Value as find criteria
           * @param options Find options
           * @returns `true` if has one or more matches with criteria, else `false`
           */
          some: Some;

          /**
           * @summary Check every items by value or value field
           * @param value Value as find criteria
           * @param options Find options
           * @returns `true` if every items matches with criteria, else `false`
           */
          every: Every;

          /**
           * @summary Filter items by value or value field
           * @param value Value as find criteria
           * @param options Find options
           * @returns New array of filtered items
           */
          filter: Filter;

          /**
           * @summary Extract items field `F`
           * @param options Find options
           * @returns New array of field `F` values
           */
          extract: Extract;

          /**
           * @summary Count of items by value or value field
           * @param options Find options
           * @returns Count of items satisfied to condition
           */
          count: Count;
        } & (W extends true
          ? {
              /**
               * @summary Sort items in original array by value or value field
               * @param value Value as sort criteria
               * @param options Sort options
               * @returns Sorted original array
               */
              sort: Sort;
            }
          : {});
        //#endregion

        //#region Key
        export namespace Key {
          const R = <
            K extends keyof Methods.Template<false>,
            A extends Algorithm
          >(
            key: K,
            algorithm: A
          ) => avstantso.JS.set.raw({}, key, algorithm) as Record<K, A>;

          const W = <
            K extends keyof Methods.Template<true>,
            A extends Algorithm
          >(
            key: K,
            algorithm: A
          ) => avstantso.JS.set.raw({}, key, algorithm) as Record<K, A>;

          export namespace Map {
            export const Std = {
              ...R('filter', 'P'),
              ...R('every', 'P'),
              ...R('some', 'P'),
              ...R('findIndex', 'P'),
              ...R('find', 'P'),
            };

            export type Std = typeof Std;

            export const StdW = {
              ...W('sort', 'S'),
              ...Std,
            };

            export type StdW = typeof StdW;
          }

          export type Std<W extends boolean> = keyof (W extends true
            ? Methods.Key.Map.StdW
            : Methods.Key.Map.Std);
          // type r = Std<false>;
          // type w = Std<true>;

          export type Custom<W extends boolean> =
            keyof typeof By.Methods.Custom;
        }

        export type Key<W extends boolean> =
          | Methods.Key.Std<W>
          | Methods.Key.Custom<W>;
        //#endregion

        //#region abstract
        export type RT<
          W extends boolean,
          T,
          N extends Methods.Key.Std<W>
        > = ReturnType<
          Extract<RW<W, T>[Extract<N, keyof RW<W, T>>], (...args: any) => any>
        >;
        // type rtR = RT<false, {X: string}, 'find'>
        // type rtW = RT<true, {X: string}, 'sort'>

        export type DefaultsOptions<
          W extends boolean,
          T,
          F extends Primitives.Field<T>
        > = {
          /**
           * @summary If array item hasn't field `F`, use this value.
           *
           * You may set hook, for calculate value or raise error
           */
          defaults?:
            | Primitives.Value.Or.Provider<T, F>
            | ((context: {
                field: string;
                type: string;
                value: unknown;
                item: T;
              }) => Primitives.Value.Or.Provider<T, F>);
        };
        //#endregion

        //#region Predicated
        export namespace Predicated {
          /**
           * @summary Predicated method options
           */
          export type Options<
            W extends boolean,
            T,
            F extends Primitives.Field<T>
          > = DefaultsOptions<W, T, F> & {
            /**
             * @summary If `truthy`, invert predicate result
             */
            notEquals?: boolean;

            /**
             * @summary If `truthy` — use `==` in predicate, `===` else
             * @default false for all fields, except boolean fields
             */
            fuzzy?: boolean;
          };

          /**
           * @summary Predicated method signature
           */
          export type Raw<
            W extends boolean,
            T,
            F extends Primitives.Field<T>,
            RT
          > = {
            /**
             * @summary Predicated method signature
             * @param value Value to search
             * @param options Search options.
             *
             * `number` as options supports for match as predicate to other array:
             * `XXX.find(YYY.by.id.find)`
             *
             * if `number` — options ignores
             */
            (
              value: Primitives.Value.Or.Provider<T, F>,
              options?: Predicated.Options<W, T, F> | number
            ): RT;

            /**
             * @summary Find with `notEquals` option is `true`
             * @param value Value to search
             * @param options Search options.
             *
             * `number` as options supports for match as predicate to other array:
             * `XXX.find(YYY.by.id.find)`
             *
             * if `number` — options ignores
             */
            neq(
              value: Primitives.Value.Or.Provider<T, F>,
              options?: Omit<Predicated.Options<W, T, F>, 'notEquals'> | number
            ): RT;
          };

          /**
           * @summary Standard `Array` predicated method with name `N`
           * @param value Value as find criteria
           * @param options Find options
           */
          export type Std<
            W extends boolean,
            T,
            F extends Primitives.Field<T>,
            N extends Methods.Key.Std<W>
          > = Raw<W, T, F, Methods.RT<W, T, N>>;
        }

        /**
         * @summary Custom `Array` predicated method
         * @param value Value as find criteria
         * @param options Find options
         */
        export type Predicated<
          W extends boolean,
          T,
          F extends Primitives.Field<T>,
          RT
        > = Predicated.Raw<W, T, F, RT>;
        //#endregion

        //#region Sort
        export namespace Sort {
          /**
           * @summary Sort method options
           */
          export type Options<
            T,
            F extends Primitives.Field<T>
          > = DefaultsOptions<true, T, F> & {
            /**
             * @summary Descending sort
             */
            desc?: boolean;

            /**
             * @summary Controls `null`, `undefined`, `NaN` sort priority. If `true` — tsey`s will be firsts, else — lasts
             * @default true
             */
            nullsFirst?: boolean;
          };

          /**
           * @summary Sort method signature
           */
          export type Raw<T, F extends Primitives.Field<T>, RT> = {
            (options?: Options<T, F>): RT;

            /**
             * @summary Sort with `desc` option is `true`
             */
            desc(options?: Omit<Options<T, F>, 'desc'>): RT;
          };
        }

        /**
         * @summary Sort method
         */

        export type Sort<T, F extends Primitives.Field<T>> = Sort.Raw<
          T,
          F,
          Methods.RT<true, T, 'sort'>
        >;
        //#endregion

        //#region Custom
        export namespace Custom {
          export namespace Extract {
            export type Options<
              W extends boolean,
              T,
              F extends Primitives.Field<T>
            > = DefaultsOptions<W, T, F>;
          }

          export type Extract<
            W extends boolean,
            T,
            F extends Primitives.Field<T>
          > = (options?: Extract.Options<W, T, F>) => Primitives.Value<T, F>[];

          export type Count<
            W extends boolean,
            T,
            F extends Primitives.Field<T>
          > = Predicated<W, T, F, number>;
        }

        export const Custom = (() => {
          //#region types
          type W = true;
          type T = any;
          type F = any;

          type SafeItemValue = (
            item: T,
            defaults: Predicated.Options<true, any, any>['defaults'],
            value?: Primitives.Value.Or.Provider<T, F>
          ) => Primitives.Value.Or.Provider<T, F>;

          type Context = {
            arr: T[];
            field: F;
            safeItemValue: SafeItemValue;
            stdAlgorithms: Algorithm.Map;
          };

          type Signature<M extends (...args: any) => any> = (
            context: Context
          ) => M;
          //#endregion

          const extract: Signature<Custom.Extract<W, T, F>> =
            ({ arr, safeItemValue }) =>
            ({ defaults } = {}) =>
              arr.map((item) => safeItemValue(item, defaults));

          const count: Signature<Custom.Count<W, T, F>> = ({
            stdAlgorithms: { P },
          }) => P('filter', (arr) => arr.length);

          return { extract, count };
        })();
        //#endregion
      }

      export type Methods<
        W extends boolean,
        T,
        F extends Primitives.Field<T>,
        R = Methods.Template<
          W,
          Methods.Predicated.Std<W, T, F, 'find'>,
          Methods.Predicated.Std<W, T, F, 'findIndex'>,
          Methods.Predicated.Std<W, T, F, 'some'>,
          Methods.Predicated.Std<W, T, F, 'every'>,
          Methods.Predicated.Std<W, T, F, 'filter'>,
          Methods.Sort<T, F>,
          Methods.Custom.Extract<W, T, F>,
          Methods.Custom.Count<W, T, F>
        >
      > = R;
    }

    // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
    export type By<
      W extends boolean,
      T,
      R = {
        <F extends Primitives.Field<T>>(field: F): By.Methods<W, T, F>;
      } & { [K in Primitives.Field<T>]: By.Methods<W, T, K> }
    > = R;
  }

  export interface Ex<W extends boolean, T> {
    /**
     * @summary By `filed` methods, if available.
     *
     * Call `by(field)` or get `by['field']` if `T` has supported fields from
     *
     * `AVStantso.Array.Ex.Field.Map.Filter<T>`
     */
    by: Ex.By<W, T>;
  }
}

declare interface Array<T> extends AVStantso.Array.Ex<true, T> {}
declare interface ReadonlyArray<T> extends AVStantso.Array.Ex<false, T> {}

!Array.prototype.by &&
  Object.defineProperties(Array.prototype, {
    by: {
      get() {
        if (!this) return undefined;

        const arr: any[] = this;

        const fieldsMap = avstantso.Primitives.Field.Map;

        function by(field: string) {
          type DefaultsOptions = AVStantso.Array.Ex.By.Methods.DefaultsOptions<
            true,
            any,
            any
          >;

          const meta = fieldsMap[
            field as keyof typeof fieldsMap
          ] as AVStantso.Primitives.Meta;

          const stdMethods = Object.entries(
            AVStantso.Array.Ex.By.Methods.Key.Map.StdW
          );
          const extMethods = Object.entries(
            AVStantso.Array.Ex.By.Methods.Custom
          );

          const byResult: any = {};

          function safeItemValue(
            item: any,
            defaults: DefaultsOptions['defaults'],
            value?: any
          ) {
            return item.hasOwnProperty(field)
              ? item[field]
              : 'function' === typeof defaults
              ? defaults({
                  field,
                  type: meta.type,
                  value,
                  item,
                })
              : defaults;
          }

          function Predicated(method: string, transform: (result: any) => any) {
            {
              type Options = AVStantso.Array.Ex.By.Methods.Predicated.Options<
                true,
                any,
                any
              >;

              function prepare(raw: unknown) {
                return raw instanceof Date ? +raw : raw;
              }

              function f(value: any, options?: Options) {
                const type = meta.type;

                const {
                  defaults,
                  notEquals = false,
                  fuzzy = 'boolean' === type ? true : false,
                } = options || {};

                const prepared = prepare(
                  'object' === typeof value && !(value instanceof Date)
                    ? value && value[field]
                    : value
                );

                const result: any = arr[method as keyof any[]](
                  (item: NodeJS.Dict<unknown>) => {
                    const itemValue = safeItemValue(item, defaults, value);

                    const r = fuzzy
                      ? prepare(itemValue) == prepared
                      : prepare(itemValue) === prepared;

                    return notEquals ? !r : r;
                  }
                );

                return transform ? transform(result) : result;
              }

              Object.defineProperties(f, {
                neq: {
                  get() {
                    function neq(value: any, options?: Options) {
                      return f(value, { ...options, notEquals: true });
                    }

                    return neq;
                  },
                },
              });

              return f;
            }
          }

          function Sort(method: string, transform: (result: any) => any) {
            type Options = AVStantso.Array.Ex.By.Methods.Sort.Options<any, any>;
            function f(options?: Options) {
              const type = meta.type;

              const {
                defaults,
                desc = false,
                nullsFirst = true,
              } = ('object' === typeof options && options) || {};

              const toNulls = avstantso.Compare.ToNulls(
                'number' === type,
                nullsFirst
              );

              const result: any = arr.sort((itemA, itemB) => {
                const a = safeItemValue(itemA, defaults);
                const b = safeItemValue(itemB, defaults);

                if (a === b) return 0;

                const r =
                  toNulls(a) - toNulls(b) ||
                  (() => {
                    const tA = typeof a;
                    const tB = typeof b;

                    if (
                      tA !== tB ||
                      ('Date' === type
                        ? !(a instanceof Date) || !(b instanceof Date)
                        : tA !== type || tB !== type)
                    )
                      throw Error(
                        `Sort by field "${field}" fails. Values types mismatch (a: "${tA}", b: "${tB}") with expected type "${type}"`
                      );

                    if ('string' === type || 'symbol' === type) {
                      const sa = String(a || '');
                      const sb = String(b || '');

                      return sa.localeCompare(sb);
                    }

                    return +a - +b;
                  })();

                return desc ? -r : r;
              });

              return transform ? transform(result) : result;
            }

            Object.defineProperties(f, {
              desc: {
                get() {
                  function desc(options?: Options) {
                    return f({ ...options, desc: true });
                  }

                  return desc;
                },
              },
            });

            return f;
          }

          const StdAlgorithms = {
            P: Predicated,
            S: Sort,
          } as AVStantso.Array.Ex.By.Algorithm.Map;

          stdMethods.forEach(([method, algorithmCode]) =>
            Object.defineProperty(byResult, method, {
              get() {
                const algorithm =
                  StdAlgorithms[algorithmCode as keyof typeof StdAlgorithms];

                return algorithm(
                  method as keyof AVStantso.Array.Ex.By.Methods.Key.Map.StdW
                );
              },
            })
          );

          extMethods.forEach(([method, impl]) =>
            Object.defineProperty(byResult, method, {
              get() {
                return impl({
                  arr,
                  field,
                  safeItemValue,
                  stdAlgorithms: StdAlgorithms,
                });
              },
            })
          );

          return byResult;
        }

        return new Proxy(by, {
          get(target, p) {
            const field = String(p);
            if (Object.keys(fieldsMap).includes(field)) return by(field);

            if (target.hasOwnProperty(p))
              return target[p as keyof typeof target];
          },
        });
      },
    },
  });
