namespace AVStantso {
  export namespace Primitives {
    export namespace Meta {
      /**
       * @summary Metadata provider of primitive
       */
      // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
      export type Provider = {
        /**
         * @summary Metadata of primitive
         */
        meta: TS.Type.KeyDef;
      };

      /**
       * @summary Map of exteranl types for `Meta`
       */
      export type ExternalTypesMap = NodeJS.Dict<unknown>;

      /**
       * @summary Get TS type from meta `T`
       * @param T Primitive meta type to resolve
       * @example
       * type u = CheckType<Meta.GetType<{}>, unknown>;
       * type r = CheckType<Meta.GetType<{ resolved: boolean }>, boolean>;
       * type c = CheckType<
       *   Meta.GetType<{ resolved: boolean; compatible: boolean | boolean[] }>,
       *   boolean | boolean[]
       * >;
       */
      export type GetType<T extends Pick<Meta.Raw, 'resolved' | 'compatible'>> =
        'compatible' extends keyof T ? T['compatible'] : T['resolved'];

      /**
       * @summary Length for string
       */
      export type Length =
        | number
        | { min: number; max?: number }
        | { min?: number; max: number };

      /**
       * @summary Raw metadata of primitive
       */
      export type Raw<T = unknown> = {
        /**
         * @summary String type metadata
         */
        type: T;

        /**
         * @summary Design-time TS type resolved from `type`
         */
        resolved?: unknown;

        /**
         * @summary Design-time TS type. If defined — used instead of `resolved`
         */
        compatible?: unknown;

        /**
         * @summary `true`, for not `TS.Type.Literal` types
         */
        external?: boolean;

        /**
         * @summary Is optional field
         */
        optional?: boolean;

        /**
         * @summary Default value
         */
        default?: unknown;

        /**
         * @summary Minimal value for number-like types
         */
        min?: number;

        /**
         * @summary Maximal value for number-like types
         */
        max?: number;

        /**
         * @summary Length for string-like types
         */
        length?: Meta.Length;

        /**
         * @summary Pattern for string-like types
         */
        pattern?: string;

        /**
         * @summary Closed list of available values
         */
        values?: unknown[];
      };
    }

    /**
     * @summary Metadata of primitive
     * @param TExternal Optional map of extended types
     */
    export type Meta<TExternal extends Meta.ExternalTypesMap = undefined> =
      Meta.Raw<TS.Type.Literal | keyof TExternal>;
  }

  export namespace Code {
    export namespace Primitives {
      export namespace Meta {
        type Key = keyof AVStantso.Primitives.Meta;

        namespace Key {
          export type Ex = Key | 'required';

          export type Optional<K = Key> = Extract<K, 'resolved' | 'compatible'>;

          export type NotOptional<K = Key> = Exclude<
            K,
            'resolved' | 'compatible'
          >;
        }

        export namespace Define {
          /**
           * @summary TypeMap for customize define
           */
          export type TypeMap = {
            Fields?: NodeJS.Dict<AVStantso.Primitives.Meta>;
            Fragments?: NodeJS.Dict<AVStantso.Primitives.Fragment>;
            External?: AVStantso.Primitives.Meta.ExternalTypesMap;
          };

          export namespace TypeMap {
            /**
             * @summary `Callback.F` function generic argument constraint
             * @see Callback.F
             */
            export type FArg<TTypeMap extends TypeMap> =
              | AVStantso.TS.Type.Literal
              | keyof TTypeMap['Fragments']
              | keyof TTypeMap['Fields']
              | keyof TTypeMap['External'];
          }

          //#region Result
          /**
           * @summary Metadata result (with object remap)
           */
          export type Result<T = unknown> = {
            _result: T extends object ? { [K in keyof T]: T[K] } : T;
          };

          export namespace Result {
            /**
             * @summary Metadata result dictionary with specific `Depth`
             * @example
             * type n = CheckType<Dict<never>, never>;
             * type u = CheckType<Dict<undefined>, undefined>;
             *
             * type d0 = CheckType<Dict<0>, Result>;
             * type d1 = CheckType<Dict<1>, NodeJS.Dict<Result>>;
             * type d2 = CheckType<Dict<2>, NodeJS.Dict<NodeJS.Dict<Result>>>;
             */
            export type Dict<Depth extends number = 1> = Depth extends undefined
              ? undefined
              : Depth extends 0
              ? Result
              : NodeJS.Dict<Dict<AVStantso.TS.Decrement<Depth>>>;
          }
          //#endregion

          /**
           * @summary Clear metadata dictionary (any depth) by resolve `Result`
           */
          export type Clear<TRawMeta> = TRawMeta extends object
            ? TRawMeta extends Result
              ? TRawMeta['_result']
              : {
                  [K in keyof TRawMeta]: Clear<TRawMeta[K]>;
                }
            : TRawMeta;

          /**
           * @summary Define metadata callback
           */
          export namespace Callback {
            /**
             * @summary `FChain` function generic argument constraint
             * @see FChain
             */
            type FCArg<T = unknown, E extends Key.Ex = Key.Ex> = {
              M: AVStantso.Primitives.Meta.Raw<T>;
              E: E;
            };

            namespace FCArg {
              /**
               * @summary `FChain` function generic argument addition
               * @see FChain
               */
              export type Add = Partial<
                Omit<AVStantso.Primitives.Meta.Raw, 'type'>
              >;

              namespace Merge {
                /**
                 * @summary `FCArg` merge prop
                 * @see FCArg
                 */
                export type Prop<
                  A extends FCArg,
                  P extends FCArg.Add,
                  K
                > = K extends keyof P
                  ? P[K]
                  : K extends keyof A['M']
                  ? A['M'][K]
                  : never;

                /**
                 * @summary `FCArg` merge part
                 * @see FCArg
                 */
                export type Part<
                  A extends FCArg,
                  P extends FCArg.Add,
                  O extends boolean,
                  KK extends keyof P | keyof A['M'] = keyof P | keyof A['M']
                > = O extends true
                  ? {
                      [K in Key.Optional<KK>]?: Merge.Prop<A, P, K>; // 👈 ?
                    }
                  : {
                      [K in Key.NotOptional<KK>]: Merge.Prop<A, P, K>; // 👈 ❌
                    };
              }

              /**
               * @summary `FCArg` argument merge with `P` and `E`
               * @see FCArg
               */
              export type Merge<
                A extends FCArg,
                P extends FCArg.Add = {},
                E extends Key = Extract<keyof P, Key>
              > = {
                M: {
                  type: A['M']['type']; // 👈 first key
                } & Merge.Part<A, P, true> &
                  Merge.Part<A, P, false>;
                E: A['E'] | E;
              };
            }

            /**
             * @summary Get `['M']['resolved']` from `FCArg`
             * @see FCArg
             */
            type Resolved<A extends FCArg> = A['M']['resolved'];

            //#region FActions
            namespace FActions {
              /**
               * @summary Define chain common actions
               */
              export type Common<A extends FCArg = undefined> = {
                /**
                 * @summary Define `compatible`
                 * @param template Dummy template for define generic `T`
                 */
                compatible<T = never>(
                  template?: T
                ): Resolved<A> extends T
                  ? FChain<A, { compatible?: T }>
                  : `${'`'}${A['M']['type'] extends string
                      ? A['M']['type']
                      : '<type>'}${'`'} must extends ${'`T`'}`;

                /**
                 * @summary Define as `optional`
                 */
                optional: FChain<A, { optional: true }>;

                /**
                 * @summary Remove `optional` from defenition
                 */
                required: FChain<
                  {
                    M: Omit<A['M'], 'optional'>;
                    E: Exclude<A['E'], 'optional'>;
                  },
                  {}
                >;

                /**
                 * @summary Define `default`
                 */
                default<D extends Resolved<A>>(
                  defaultValue: D
                ): FChain<A, { default: D }>;
              };

              /**
               * @summary Define chain common literal type actions
               */
              export type CommonLiteral<A extends FCArg = undefined> = {
                /**
                 * @summary Define closed list of available values
                 */
                values: {
                  <
                    V00 extends Resolved<A>,
                    V01 extends Resolved<A> = undefined,
                    V02 extends Resolved<A> = undefined,
                    V03 extends Resolved<A> = undefined,
                    V04 extends Resolved<A> = undefined
                  >(
                    ...values: [V00, V01?, V02?, V03?, V04?]
                  ): FChain<
                    A,
                    { values: AVStantso.TS.Array.FilterUnique<typeof values> }
                  >;

                  <
                    V00 extends Resolved<A>,
                    V01 extends Resolved<A> = undefined,
                    V02 extends Resolved<A> = undefined,
                    V03 extends Resolved<A> = undefined,
                    V04 extends Resolved<A> = undefined,
                    V05 extends Resolved<A> = undefined,
                    V06 extends Resolved<A> = undefined,
                    V07 extends Resolved<A> = undefined,
                    V08 extends Resolved<A> = undefined,
                    V09 extends Resolved<A> = undefined
                  >(
                    ...values: [
                      V00,
                      V01?,
                      V02?,
                      V03?,
                      V04?,
                      V05?,
                      V06?,
                      V07?,
                      V08?,
                      V09?
                    ]
                  ): FChain<
                    A,
                    { values: AVStantso.TS.Array.FilterUnique<typeof values> }
                  >;

                  <
                    V00 extends Resolved<A>,
                    V01 extends Resolved<A> = undefined,
                    V02 extends Resolved<A> = undefined,
                    V03 extends Resolved<A> = undefined,
                    V04 extends Resolved<A> = undefined,
                    V05 extends Resolved<A> = undefined,
                    V06 extends Resolved<A> = undefined,
                    V07 extends Resolved<A> = undefined,
                    V08 extends Resolved<A> = undefined,
                    V09 extends Resolved<A> = undefined,
                    V10 extends Resolved<A> = undefined,
                    V11 extends Resolved<A> = undefined,
                    V12 extends Resolved<A> = undefined,
                    V13 extends Resolved<A> = undefined,
                    V14 extends Resolved<A> = undefined,
                    V15 extends Resolved<A> = undefined,
                    V16 extends Resolved<A> = undefined,
                    V17 extends Resolved<A> = undefined,
                    V18 extends Resolved<A> = undefined,
                    V19 extends Resolved<A> = undefined
                  >(
                    ...values: [
                      V00,
                      V01?,
                      V02?,
                      V03?,
                      V04?,
                      V05?,
                      V06?,
                      V07?,
                      V08?,
                      V09?,
                      V10?,
                      V11?,
                      V12?,
                      V13?,
                      V14?,
                      V15?,
                      V16?,
                      V17?,
                      V18?,
                      V19?,
                      ...Resolved<A>[]
                    ]
                  ): FChain<
                    A,
                    { values: AVStantso.TS.Array.FilterUnique<typeof values> }
                  >;
                };
              };

              /**
               * @summary Define chain number-like actions
               */
              export type NumberLike<A extends FCArg = undefined> = {
                /**
                 * @summary Define number-like minimal value
                 */
                min<VMin extends number>(min: VMin): FChain<A, { min: VMin }>;

                /**
                 * @summary Define number-like maximal value
                 */
                max<VMax extends number>(min: VMax): FChain<A, { max: VMax }>;
              };

              /**
               * @summary Define chain string-like actions
               */
              export type StringLike<A extends FCArg = undefined> = {
                /**
                 * @summary Define string-like `length`
                 */
                length: {
                  /**
                   * @summary Define string-like fixed `length`
                   */
                  <LN extends number>(length: LN): FChain<A, { length: LN }>;

                  /**
                   * @summary Define string-like `length` with `min` and `max`
                   */
                  <LMin extends number, LMax extends number>(
                    min: LMin,
                    max: LMax
                  ): FChain<A, { length: { min: LMin; max: LMax } }>;

                  /**
                   * @summary Define string-like minimal `length`
                   */
                  min<LMin extends number>(
                    min: LMin
                  ): FChain<A, { length: { min: LMin } }>;

                  /**
                   * @summary Define string-like maximal `length`
                   */
                  max<LMax extends number>(
                    min: LMax
                  ): FChain<A, { length: { max: LMax } }>;
                };

                /**
                 * @summary Define string-like `pattern`
                 */
                pattern<P extends string>(
                  pattern: P
                ): FChain<A, { pattern: P }>;
              };
            }

            /**
             * @summary Define chain actions
             */
            type FActions<
              A extends FCArg = undefined,
              T = A['M']['type']
            > = Omit<
              FActions.Common<A> &
                (T extends AVStantso.TS.Type.Literal
                  ? FActions.CommonLiteral<A> &
                      (AVStantso.TS.Type.Literal.IsNumberLike<T> extends true
                        ? FActions.NumberLike<A>
                        : {}) &
                      (AVStantso.TS.Type.Literal.IsStringLike<T> extends true
                        ? FActions.StringLike<A>
                        : {})
                  : {}),
              A['E'] | ('optional' extends keyof A['M'] ? never : 'required')
            >;
            //#endregion

            //#region FSetMetaFunction
            /**
             * @summary Define metadata fully. Ends chain
             */
            type FSetMetaFunction<
              A extends FCArg,
              T = A['M']['type'],
              CK = keyof FActions.CommonLiteral,
              NK = keyof FActions.NumberLike,
              SK = keyof FActions.StringLike,
              N =
                | 'type'
                | 'external'
                | Key.Optional
                | A['E']
                | (T extends AVStantso.TS.Type.Literal
                    ?
                        | AVStantso.TS.Type.Literal.IsNumberLike<T, never, NK>
                        | AVStantso.TS.Type.Literal.IsStringLike<T, never, SK>
                    : CK | NK | SK),
              K extends Key = Exclude<Key, N>
            > = [K] extends [never]
              ? {}
              : {
                  <
                    M extends Pick<AVStantso.Primitives.Meta.Raw, K> &
                      Partial<Record<Extract<N, AVStantso.TS.Key>, never>>
                  >(
                    meta: M
                  ): FChain<A, M>;
                };
            //#endregion

            /**
             * @summary Define metadata callback argument function chain
             */
            type FChain<
              A extends FCArg,
              P extends FCArg.Add = {},
              E extends Key = Extract<keyof P, Key>,
              N extends FCArg = FCArg.Merge<A, P, E>
            > = Result<N['M']> & FSetMetaFunction<N> & FActions<N>;

            /**
             * @summary Define metadata callback argument function begin chain
             */
            type FBeginChain<
              TTypeMap extends TypeMap,
              T extends TypeMap.FArg<TTypeMap>,
              TM extends TypeMap = TTypeMap extends undefined ? {} : TTypeMap
            > = T extends keyof TM['Fragments']
              ? {
                  [K in keyof TM['Fragments'][T]]: Result<
                    TM['Fragments'][T][K]
                  >;
                }
              : T extends keyof TM['Fields']
              ? FChain<{
                  M: Extract<TM['Fields'][T], AVStantso.Primitives.Meta.Raw>;
                  E: never;
                }>
              : FChain<{
                  M: {
                    type: T;
                    resolved?: T extends AVStantso.TS.Type.Literal
                      ? AVStantso.TS.Type.Resolve<T>
                      : T extends keyof TM['External']
                      ? TM['External'][T]
                      : never;
                  } & (T extends keyof TM['External']
                    ? { external: true }
                    : {});
                  E: never;
                }>;

            /**
             * @summary Define metadata callback argument function
             */
            export type F<TTypeMap extends TypeMap> = (<
              T extends TypeMap.FArg<TTypeMap>
            >(
              type: T
            ) => FBeginChain<TTypeMap, T>) & {
              [K in AVStantso.TS.Type.Literal]: FBeginChain<TTypeMap, K>;
            } & (TTypeMap extends undefined
                ? {}
                : AVStantso.TS.Flat<{
                    [F in keyof TTypeMap]: {
                      [K in keyof TTypeMap[F]]: FBeginChain<
                        TTypeMap,
                        Extract<K, TypeMap.FArg<TTypeMap>>
                      >;
                    };
                  }>);
          }
        }

        /**
         * @summary Define metadata
         * @param TTypeMap TypeMap for customize define
         * @param TRawResultStructOrDepth Raw result structure or `Depth` for `Define.Result.Dict`
         * @example
         * let D: Define;
         * const { active0, active1, active2, active3, active4 } = D((f) => ({
         *   active0: f('boolean')({ default: true, optional: true }),
         *   // {type: 'boolean'; resolved?: boolean; default: boolean; optional: true}
         *
         *   active1: f.boolean({ default: true, optional: true }),
         *   // {type: 'boolean'; resolved?: boolean; default: boolean; optional: true}
         *
         *   active2: f.boolean.optional({ default: true }),
         *   // {type: 'boolean'; resolved?: boolean; optional: true; default: boolean}
         *
         *   active3: f.boolean.optional.default(true),
         *   // {type: 'boolean'; resolved?: boolean; optional: true; default: true}
         *
         *   active4: f.boolean.default(true).optional,
         *   // {type: 'boolean'; resolved?: boolean; default: true; optional: true}
         * }));
         * type active4 = CheckType<
         *   typeof active4,
         *   {
         *     type: 'boolean';
         *     resolved?: boolean;
         *     optional: true;
         *     default: true;
         *   }
         * >;
         *
         * const { name } = D((f) => ({
         *   name: f.string
         *     .length(3, 30)
         *     .pattern('^[A-Za-z][A-Za-z0-9_]{2,29}$'),
         * }));
         * type name = CheckType<
         *   typeof name,
         *   {
         *     type: 'string';
         *     resolved?: string;
         *     length: {
         *       min: 3;
         *       max: 30;
         *     };
         *     pattern: '^[A-Za-z][A-Za-z0-9_]{2,29}$';
         *   }
         * >;
         *
         * const { description } = D((f) => ({
         *   description: f.string.optional.length.max(256),
         * }));
         * type description = CheckType<
         *   typeof description,
         *   {
         *     type: 'string';
         *     resolved?: string;
         *     length: {
         *       max: 256;
         *     };
         *     optional: true;
         *   }
         * >;
         *
         * const { token } = D((f) => ({
         *   token: f.Buffer.length(64).pattern('^(d|[A-Fa-f]){64}$'),
         * }));
         * type token = CheckType<
         *   typeof token,
         *   {
         *     type: 'Buffer';
         *     resolved?: Buffer;
         *     length: 64;
         *     pattern: '^(d|[A-Fa-f]){64}$';
         *   }
         * >;
         *
         * const { dice0, dice1 } = D((f) => ({
         *   dice0: f.number({ min: 1, max: 6 }),  // {type: 'number'; resolved?: number; min: number; max: number}
         *   dice1: f.number.min(1).max(6),        // {type: 'number'; resolved?: number; min: 1; max: 6}
         * }));
         * type dice1 = CheckType<
         *   typeof dice1,
         *   {
         *     type: 'number';
         *     resolved?: number;
         *     min: 1;
         *     max: 6;
         *   }
         * >;
         *
         * const { one_two_three } = D((f) => ({
         *   one_two_three: f.string.values('one', 'two', 'three', 'one'), // duplicates ignored
         * }));
         * type one_two_three = CheckType<
         *   typeof one_two_three,
         *   {
         *     type: 'string';
         *     resolved?: string;
         *     values: ['one', 'two', 'three'];
         *   }
         * >;
         *
         * const { accept_encoding } = D((f) => ({
         *   accept_encoding: f.string.compatible<string | string[]>()
         *     .optional,
         * }));
         * type accept_encoding = CheckType<
         *   typeof accept_encoding,
         *   {
         *     type: 'string';
         *     resolved?: string;
         *     compatible?: string | string[];
         *     optional: true;
         *   }
         * >;
         * type accept_encoding_type = CheckType<
         *   AVStantso.Primitives.Meta.GetType<typeof accept_encoding>,
         *   string | string[]
         * >;
         *
         * // External
         * let DE: Define<{ External: { mind: number } }>;
         * const { mind0, mind1 } = DE((f) => ({
         *   mind0: f('mind').default(45),  // {type: "mind"; resolved?: number; external: true; default: 45;}
         *   mind1: f.mind.default(48),     // {type: "mind"; resolved?: number; external: true; default: 48;}
         * }));
         * type mind1 = CheckType<
         *   typeof mind1,
         *   {
         *     type: 'mind';
         *     resolved?: number;
         *     external: true;
         *     default: 48;
         *   }
         * >;
         *
         * // Fields
         * let DFd: Define<{ Fields: { active4: typeof active4 } }>;
         * const { active5, active6, active7 } = DFd((f) => ({
         *   active5: f('active4').default(false),
         *   // {type: "boolean"; resolved?: boolean; optional: true; default: false}
         *
         *   active6: f.active4.default(false),
         *   // {type: "boolean"; resolved?: boolean; optional: true; default: false}
         *
         *   active7: f.active4.default(false).required,
         *   // {type: "boolean"; resolved?: boolean; default: false}
         * }));
         * type active7 = CheckType<
         *   typeof active7,
         *   {
         *     type: 'boolean';
         *     resolved?: boolean;
         *     default: false;
         *   }
         * >;
         *
         * // Fragments
         * const IDed = D(({ string }) => ({
         *   id: string,
         * }));
         * type IDed = CheckType<
         *   typeof IDed,
         *   {
         *     id: {
         *       type: 'string';
         *       resolved?: string;
         *     };
         *   }
         * >;
         *
         * const Dates = D(({ Date }) => ({
         *   putdate: Date,
         *   upddate: Date,
         * }));
         * type Dates = CheckType<
         *   typeof Dates,
         *   {
         *     putdate: {
         *       type: 'Date';
         *       resolved?: Date;
         *     };
         *     upddate: {
         *       type: 'Date';
         *       resolved?: Date;
         *     };
         *   }
         * >;
         *
         * const Versioned = D(({ number }) => ({
         *   version: number.default(0),
         * }));
         * type Versioned = CheckType<
         *   typeof Versioned,
         *   {
         *     version: {
         *       type: 'number';
         *       resolved?: number;
         *       default: 0;
         *     };
         *   }
         * >;
         *
         * let DFg: Define<{
         *   Fragments: {
         *     IDed: IDed;
         *     Dates: Dates;
         *     Versioned: Versioned;
         *   };
         * }>;
         *
         * const Select = DFg(({ IDed, Dates, Versioned }) => ({
         *   ...Versioned,
         *   ...Dates,
         *   ...IDed,
         * }));
         * type Select = CheckType<
         *   typeof Select,
         *   {
         *     id: {
         *       type: 'string';
         *       resolved?: string;
         *     };
         *     putdate: {
         *       type: 'Date';
         *       resolved?: Date;
         *     };
         *     upddate: {
         *       type: 'Date';
         *       resolved?: Date;
         *     };
         *     version: {
         *       type: 'number';
         *       resolved?: number;
         *       default: 0;
         *     };
         *   }
         * >;
         */
        export type Define<
          TTypeMap extends Define.TypeMap = undefined,
          TRawResultStructOrDepth extends NodeJS.Dict<any> | number = 1
        > = <
          TRawMeta extends TRawResultStructOrDepth extends number
            ? Define.Result.Dict<TRawResultStructOrDepth>
            : TRawResultStructOrDepth
        >(
          callback: (f: Define.Callback.F<TTypeMap>) => TRawMeta
        ) => Define.Clear<TRawMeta>;

        /**
         * @summary Make define metadata for `typeMap`
         * @param TRawResultStructOrDepth Raw result structure or `Depth` for `Define.Result.Dict`
         * @see Define
         */
        // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
        export type MakeDefineFor<
          TRawResultStructOrDepth extends NodeJS.Dict<any> | number = 1
        > = {
          /**
           * @summary Make define metadata for `typeMap`
           * @param typeMap TypeMap for customize define
           * @see Define
           */
          <TTypeMap extends Meta.Define.TypeMap = undefined>(
            typeMap?: TTypeMap
          ): Define<TTypeMap, TRawResultStructOrDepth>;
        };

        /**
         * @summary Make define metadata
         * @param TRawResultStructOrDepth Raw result structure or `Depth` for `Define.Result.Dict`
         * @see Define
         */
        // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
        export type MakeDefine<
          TRawResultStructOrDepth extends NodeJS.Dict<any> | number = 1
        > = Define<undefined, TRawResultStructOrDepth> & {
          /**
           * @summary Make define metadata for `typeMap`
           * @param typeMap TypeMap for customize define
           * @see Define
           */
          For: MakeDefineFor<TRawResultStructOrDepth>;
        };
      }

      /**
       * @summary `AVStantso.Primitives.Meta` helpers utility
       */
      export interface Meta {
        /**
         * @summary Define many fragments metadata
         * @see Meta.Define
         */
        DefineFragments: Meta.MakeDefine<2>;

        /**
         * @summary Define one fragment metadata
         * @see Meta.Define
         */
        DefineFragment: Meta.MakeDefine<1>;
      }
    }

    export interface Primitives {
      /**
       * @summary `AVStantso.Primitives.Meta` helpers utility
       */
      Meta: Primitives.Meta;
    }
  }

  avstantso._reg.Primitives('Meta', ({ Generics, TS: { Type }, JS }) => {
    const MetaKeys = [
      'type',
      'external',
      'optional',
      'default',
      'min',
      'max',
      'length',
      'pattern',
      'values',
    ];

    const fragmentToRaw = <
      TRaw extends Code.Primitives.Meta.Define.Result.Dict
    >(
      fragment: AVStantso.Primitives.Fragment
    ) =>
      Object.entries(fragment).reduce(
        (r, [k, v]) => JS.set(r, k, { _result: v }),
        {} as TRaw
      );

    function clear<TRawMeta>(
      raw: TRawMeta
    ): Code.Primitives.Meta.Define.Clear<TRawMeta> {
      if (!JS.is.function(raw) && !JS.is.object(raw))
        return Generics.Cast.To(raw);

      if ('_result' in raw) {
        const { _result } : any = raw;

        const r: any = {};

        MetaKeys.forEach((k) => {
          if (!(k in _result)) return;

          let v: unknown = JS.get.raw(_result, k);

          // resolve conflict with Function.length property
          if ('length' === k) {
            type Length = Extract<AVStantso.Primitives.Meta.Length, object>;

            if (!JS.is.object<Length>(v)) return;

            const { min, max } = v;

            const hMin = JS.is.number(min);
            const hMax = JS.is.number(max);

            if (!hMin && !hMax)
              throw Error(
                `Unexpected primitive metadata "length" value: ` +
                  `${JSON.stringify(v)}`
              );

            if (min === max) v = min;
            else v = { ...(hMin ? { min } : {}), ...(hMax ? { max } : {}) };
          }

          // fix values duplicates
          else if ('values' === k) {
            const values = [...(v as any[])];

            for (let i = 0; i < values.length - 1; i++)
              for (let j = i + 1; j < values.length; j++)
                if (values[i].valueOf() === values[j].valueOf())
                  values.splice(j, 1);

            v = values;
          }

          // omit optional=false
          else if ('optional' === k) {
            if (!v) return;
          }

          r[k] = v;
        });

        return r;
      }

      return Object.entries(raw).reduce<any>(
        (r, [k, v]) => JS.set(r, k, clear(v)),
        {}
      );
    }

    function MakeDefine<
      TRawResultStructOrDepth extends NodeJS.Dict<any> | number
    >(
      rRSoD?: TRawResultStructOrDepth
    ): Code.Primitives.Meta.MakeDefine<TRawResultStructOrDepth> {
      const Define: Code.Primitives.Meta.MakeDefineFor<
        TRawResultStructOrDepth
      > = (typeMap?) => {
        function FChain(parentMeta: Primitives.Meta<typeof typeMap>): any {
          function fChain(meta: Primitives.Meta<typeof typeMap>) {
            const { length, ...rest } = meta;

            return FChain({
              ...parentMeta,
              ...rest,

              // avoid conflict with Function.length property, always use object
              ...(undefined === length
                ? {}
                : {
                    length: JS.is.number(length)
                      ? { min: length, max: length }
                      : length,
                  }),
            });
          }

          Object.defineProperties(fChain, {
            _result: { value: parentMeta },

            compatible: { value: () => FChain(parentMeta) },

            optional: { get: () => FChain({ ...parentMeta, optional: true }) },

            required: {
              get: () => {
                const { optional, ...rest } = parentMeta;
                return FChain({ ...rest });
              },
            },

            default: {
              value: (v: any) => FChain({ ...parentMeta, default: v }),
            },

            values: {
              value: (...values: any[]) => FChain({ ...parentMeta, values }),
            },

            min: { value: (min: any) => FChain({ ...parentMeta, min }) },
            max: { value: (max: any) => FChain({ ...parentMeta, max }) },

            length: {
              get: () => {
                function length(...params: [number, number?]) {
                  return FChain({
                    ...parentMeta,
                    length: { min: params[0], max: params.peek() }, // avoid conflict with Function.length property, always use object
                  });
                }

                length.min = (min: number) =>
                  FChain({
                    ...parentMeta,
                    length: {
                      ...(!JS.is.object(parentMeta.length)
                        ? {}
                        : parentMeta.length),
                      min,
                    },
                  });

                length.max = (max: number) =>
                  FChain({
                    ...parentMeta,
                    length: {
                      ...(JS.is.number(parentMeta.length)
                        ? {}
                        : parentMeta.length),
                      max,
                    },
                  });

                return length;
              },
            },

            pattern: {
              value: (pattern: string) => FChain({ ...parentMeta, pattern }),
            },
          });

          return fChain;
        }

        const externalKeys = Object.keys(typeMap?.External || {});

        const f: any = new Proxy(
          (type: any) =>
            FChain({
              type,
              ...(externalKeys.includes(type) ? { external: true } : {}),
            }),
          {
            get(target, p) {
              if (Type.Literal.hasOwnProperty(p))
                return target(JS.get.raw(Type.Literal, p));

              if (typeMap) {
                if (typeMap.Fragments?.hasOwnProperty(p))
                  return fragmentToRaw(JS.get.raw(typeMap.Fragments, p));

                if (typeMap.Fields?.hasOwnProperty(p))
                  return FChain(JS.get.raw(typeMap.Fields, p));

                if (typeMap.External?.hasOwnProperty(p))
                  return FChain({ type: Generics.Cast.To(p), external: true });
              }

              return JS.get.raw(target, p);
            },
          }
        );

        return (callback) => clear(callback(f));
      };

      const define: any = Define();
      define.For = Define;

      return define;
    }

    return {
      DefineFragments: MakeDefine(2),
      DefineFragment: MakeDefine(1),
    };
  });
}
