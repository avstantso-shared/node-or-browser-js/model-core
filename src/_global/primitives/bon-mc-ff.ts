namespace AVStantso {
  const DFs = avstantso.Primitives.Meta.DefineFragments;
  const DF = avstantso.Primitives.Meta.DefineFragment;

  const BonMCFragmentsLow = DFs(({ string }) => ({
    IDed: { id: string },
    Named: { name: string },
    Logined: { login: string },
  }));

  const BonMCFieldsLow = { ...avstantso.TS.Type.flat(BonMCFragmentsLow) };

  const BonMCFragments = {
    ...DFs.For({ Fields: BonMCFieldsLow })(
      ({ boolean, string, number, Date, id, login }) => ({
        Activable: { active: boolean.optional.default(true) },

        'Authored.ID': { author_id: id },

        'Authored.Login': { author: login },

        Dates: { putdate: Date, upddate: Date },

        Described: { desctiption: string.optional },

        Expirable: { expires: Date },

        Moduled: { module: string },

        Systemic: { system: boolean },

        Versioned: { version: number.default(0) },
      })
    ),
    ...BonMCFragmentsLow,
  };

  const BonMCFields = {
    ...DF.For({ Fields: BonMCFieldsLow })(({ login }) => ({ user: login })),

    ...avstantso.TS.Type.flat(BonMCFragments),
    ...BonMCFieldsLow,
  };

  avstantso.Primitives.Fragment.Map._extend(BonMCFragments);
  avstantso.Primitives.Field.Map._extend(BonMCFields);

  export namespace Primitives {
    // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
    export namespace Fragment {
      export namespace Map {
        export namespace BoN {
          /**
           * @summary `@avstantso/node-or-browser-js--model-core` primitives fragments map
           */
          // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
          export type ModelCore = typeof BonMCFragments;
        }
      }

      export interface Map extends Map.BoN.ModelCore {}
    }

    // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
    export namespace Field {
      export namespace Map {
        export namespace BoN {
          /**
           * @summary `@avstantso/node-or-browser-js--model-core` primitives fields map
           */
          // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
          export type ModelCore = typeof BonMCFields;
        }
      }

      export interface Map extends Map.BoN.ModelCore {}
    }
  }
}
