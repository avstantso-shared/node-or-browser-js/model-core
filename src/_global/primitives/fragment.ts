namespace AVStantso {
  export namespace Primitives {
    export namespace Fragment {
      /**
       * @summary Primitives fragments map
       */
      export interface Map {}
    }

    /**
     * @summary Primitives fragment
     */
    export type Fragment = NodeJS.Dict<AVStantso.Primitives.Meta>;
  }

  export namespace Code {
    export namespace Primitives {
      export namespace Fragment {
        /**
         * @summary `AVStantso.Primitives.Fragment.Map` helpers utility
         */
        export interface Map extends AVStantso.Primitives.Fragment.Map {
          /**
           * @summary Extend primitives fragments map by `definitions`
           * @param definitions definitions for extends primitives fragments map
           */
          _extend(
            definitions: NodeJS.Dict<AVStantso.Primitives.Fragment>
          ): void;
        }
      }

      /**
       * @summary `AVStantso.Primitives.Fragment` helpers utility
       */
      export interface Fragment {
        /**
         * @summary Primitives fields map of all registered fields definitions
         */
        Map: Fragment.Map;
      }
    }

    export interface Primitives {
      /**
       * @summary `AVStantso.Primitives.Fragment` helpers utility
       */
      Fragment: Primitives.Fragment;
    }
  }

  avstantso._reg.Primitives('Fragment', ({ JS, Primitives: { Field } }) => {
    const Map: any = {};

    const _extend: AVStantso.Code.Primitives.Fragment.Map['_extend'] = (
      definitions
    ) =>
      Object.entries(definitions).forEach(([fragment, fragmentMeta]) => {
        if (Map.hasOwnProperty(fragment)) {
          const old = Map[fragment];

          Object.entries(fragmentMeta).forEach(([field, newMeta]) => {
            const oldMeta: AVStantso.Primitives.Meta = JS.get.raw(old, field);

            if (oldMeta.type !== newMeta.type) {
              const e: any = Error(
                `Primitives fragment "${String(fragment)}" field "${String(
                  field
                )}" ("${
                  newMeta.type
                }") conflicts with already registred in map fragment field ("${
                  oldMeta.type
                }")`
              );

              e.context = {
                fragment,
                field,
                old,
                new: fragmentMeta,
              };

              throw e;
            }
          });
        }

        Map[fragment] = fragmentMeta;

        Field.Map._extend(fragmentMeta);
      });

    Map[_extend.name] = _extend;

    return { Map };
  });
}
