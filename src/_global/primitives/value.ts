namespace AVStantso {
  export namespace Primitives {
    export namespace Value {
      /**
       * @summary Primitive value provider from `T` by fieldname `F`
       */
      export type Provider<
        T,
        F extends Primitives.Field<T>,
        M extends Primitives.Field.Map.Filter<
          T,
          true
        > = Primitives.Field.Map.Filter<T, true>,
        R = { [K in Extract<F, keyof M>]: M[K] }
      > = R;

      /**
       * @summary Primitive value from `T` by fieldname `F` or...
       */
      export namespace Or {
        /**
         * @summary Primitive value from `T` by fieldname `F` or primitive value provider
         */
        export type Provider<
          T,
          F extends Primitives.Field<T>,
          R = Value<T, F> | Value.Provider<T, F>
        > = R;
      }
    }

    /**
     * @summary Primitive value from `T` by fieldname `F`
     */
    export type Value<
      T,
      F extends Primitives.Field<T>,
      M extends Primitives.Field.Map.Filter<
        T,
        true
      > = Primitives.Field.Map.Filter<T, true>,
      R = M[Extract<F, keyof M>]
    > = R;
  }
}
