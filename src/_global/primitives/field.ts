namespace AVStantso {
  export namespace Primitives {
    // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
    export namespace Field {
      export namespace Map {
        /**
         * @summary Resolved primitives fields map
         */
        // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
        export type Resolved = TS.Type.Complex.Resolve.Structure<Map>;

        /**
         * @summary Primitives fields map filtered for `T`
         */
        export type Filter<
          T,
          IsResolved extends boolean,
          R = {
            [K in Extract<keyof Map, keyof T>]: IsResolved extends true
              ? Map.Resolved[K]
              : Map[K];
          }
        > = R;
      }

      /**
       * @summary Primitives fields map
       */
      export interface Map {}
    }

    /**
     * @summary All available primitives fields, if `T` is `never`, else filtered for `T` primitives fields
     */
    // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
    export type Field<T = never> = keyof ([T] extends [never]
      ? Field.Map
      : Field.Map.Filter<T, false>);
  }

  export namespace Code {
    export namespace Primitives {
      export namespace Field {
        /**
         * @summary `AVStantso.Primitives.Field.Map` helpers utility
         */
        export interface Map extends AVStantso.Primitives.Field.Map {
          /**
           * @summary Extend primitives fields map by `definitions`
           * @param definitions definitions for extends primitives fields map
           */
          _extend(definitions: NodeJS.Dict<AVStantso.Primitives.Meta>): void;
        }
      }

      /**
       * @summary `AVStantso.Primitives.Field` helpers utility
       */
      export interface Field {
        /**
         * @summary Primitives fields map of all registered fields definitions
         */
        Map: Field.Map;
      }
    }

    export interface Primitives {
      /**
       * @summary `AVStantso.Primitives.Field` helpers utility
       */
      Field: Primitives.Field;
    }
  }

  avstantso._reg.Primitives('Field', () => {
    const Map: any = {};

    const _extend: AVStantso.Code.Primitives.Field.Map['_extend'] = (
      definitions
    ) =>
      Object.entries(definitions).forEach(([field, meta]) => {
        if (field in Map) {
          const old: Primitives.Meta = Map[field];

          if (old.type !== meta.type) {
            const e: any = Error(
              `Primitives field "${String(field)}" ("${
                meta.type
              }") conflicts with already registred in map ("${old.type}")`
            );

            e.context = { field, old, new: meta };

            throw e;
          }
        }

        Map[field] = meta;
      });

    Map[_extend.name] = _extend;

    return { Map };
  });
}
