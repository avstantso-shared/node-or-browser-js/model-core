namespace AVStantso {
  /**
   * @summary Primitives: key-value parts of model
   */
  export namespace Primitives {}

  export namespace Code {
    /**
     * @summary Primitives helpers utility
     */
    export interface Primitives {}
  }

  export interface Code {
    /**
     * @summary Primitives helpers utility
     */
    Primitives: Code.Primitives;
  }

  export const Primitives = avstantso._reg('Primitives', {});
}
