namespace AVStantso {
  export namespace Primitives {
    export namespace Utils {
      /**
       * @summary Make primitive utils by Provider
       * @param F First `Provider` param
       * @param S Second `Provider` param
       * @see Provider type
       */
      // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
      export type Make<F extends Provider.First, S = undefined> = Utils<
        Provider<F, S>
      >;

      /**
       * @summary Primitive utils map by `Primitives.Field.Map` interface
       * @see Primitives.Field.Map type
       */
      // @ts-ignore // conflict with "dist" // Duplicate identifier .ts(2300)
      export type Map = {
        [K in keyof Field.Map]: Make<
          K,
          Meta.GetType<Extract<Field.Map[K], Meta>>
        >;
      };
    }

    /**
     * @summary Primitive utils by Provider
     * @param P `Provider`
     * @see Provider type
     */
    export interface Utils<P extends Provider.Constraint> {
      /**
       * @summary Is object a `P`
       */
      Is: Provider.Is<P>;

      /**
       * @summary Equality predicate for `P` objects
       */
      Predicate: Equality.Predicate.Factory<Provider.Union<P>>;

      /**
       * @summary Extract value from `P` objects
       */
      Extract: Provider.Extract<P>;

      /**
       * @summary Compare two `P` objects
       */
      Compare: Provider.Compare<P>;

      /**
       * @summary Equals two `P` objects
       */
      Equals: Equality<Provider.Union<P>>;

      /**
       * @summary Force cast to `Partial<P>` object
       */
      Force: Generics.Cast.Force<P>;
    }
  }

  export namespace Code {
    export interface Primitives {
      /**
       * @summary `AVStantso.Primitives.Utils` helpers utility
       */
      Utils: AVStantso.Primitives.Utils.Map;
    }
  }

  avstantso._reg.Primitives(
    'Utils',
    ({ Generics, TS, JS, Provider, Equality, Primitives: { Field, Meta } }) => {
      type P = AVStantso.Provider.Constraint;
      type U = AVStantso.Primitives.Utils<P>;
      type M = AVStantso.Primitives.Meta.Provider;

      function UtilsFor(field: string): U & M {
        const arr = TS.Type.Arr(
          field,
          Generics.Cast.To(JS.get.raw(Field.Map, field).type)
        );

        const Is: U['Is'] = Provider.Is(arr);
        const Extract: U['Extract'] = Provider.Extract(arr);
        const Compare: U['Compare'] = Provider.Compare(arr, {
          extract: Extract,
        });
        const Equals: U['Equals'] = Equality.From(Compare);
        const Predicate: U['Predicate'] = Equality.Predicate.Factory(Equals);
        const Force: U['Force'] = Generics.Cast.Force.Factory();

        return {
          meta: { K: arr[0], L: arr[1] },
          Is,
          Extract,
          Compare,
          Equals,
          Predicate,
          Force,
        };
      }

      const utils = {} as AVStantso.Primitives.Utils.Map;

      return new Proxy(utils, {
        get(target, p) {
          if (!target.hasOwnProperty(p)) {
            if (!Field.Map.hasOwnProperty(p)) return undefined;

            JS.set.raw(target, p, UtilsFor(String(p)));
          }

          return JS.get.raw(target, p);
        },
      });
    }
  );
}
