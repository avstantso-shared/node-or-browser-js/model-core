import {
  JS,
  Enum,
  Provider,
  Compare,
  FlagsDecorator,
} from '@avstantso/node-or-browser-js--utils';
import { FIELDS } from './consts';

export namespace DataDecorator {
  export type Converter<TValue = any> = (field: TValue) => TValue;
  export type Custom<T> = (...fieldNames: string[]) => Readonly<Stage<T>>;

  export namespace Array {
    export type Options<TItem = unknown> = {
      sort?: Provider.Compare<TItem> | true;
    };
  }

  export type Array<T> = {
    <TItem = unknown>(
      options: Array.Options<TItem>,
      ...fieldNames: string[]
    ): Readonly<Stage<T>>;
  } & Custom<T>;

  export type Setup<T = any> = (decorator: Stage<T>) => any;

  export namespace Stage {
    export namespace Print {
      export type View<T = any> = (result: T) => unknown;

      export type Options<T = any> = {
        view?: View<T>;
        prefix?: string;
      };
    }

    export type Print<T = any> = {
      (options?: Print.Options<T>): Readonly<Stage<T>>;
      (view: Print.View<T>): Readonly<Stage<T>>;
      (prefix: string): Readonly<Stage<T>>;
    };
  }

  export interface Stage<T = any> {
    print: Stage.Print<T>;

    Dates: Readonly<Stage<T>>;
    Activable: Readonly<Stage<T>>;
    Described: Readonly<Stage<T>>;
    Systemic: Readonly<Stage<T>>;
    Select: Readonly<Stage<T>>;

    bool: DataDecorator.Custom<T>;
    number: DataDecorator.Custom<T>;
    noEmpty: DataDecorator.Custom<T>;
    date: DataDecorator.Custom<T>;
    array: DataDecorator.Array<T>;

    enum<TEnumTypeMap extends Enum.TypeMap>(
      enumeration: Enum<TEnumTypeMap>,
      ...fieldNames: string[]
    ): Readonly<Stage<T>>;
    flags: {
      <TMap extends FlagsDecorator.Map>(
        map: TMap,
        ...fieldNames: string[]
      ): Readonly<Stage<T>>;

      <
        TMap extends FlagsDecorator.Map,
        TFlagsFieldName extends string = 'flags'
      >(
        map: TMap,
        options: FlagsDecorator.Options,
        ...fieldNames: string[]
      ): Readonly<Stage<T>>;

      (fd: FlagsDecorator): Readonly<Stage<T>>;
    };
    convert(
      cnv: DataDecorator.Converter,
      ...fieldNames: string[]
    ): Readonly<Stage<T>>;
    mapArray(
      mapper: DataDecorator.Converter,
      ...fieldNames: string[]
    ): Readonly<Stage<T>>;
  }
}

export type DataDecorator<T = any> = (data: T) => T;

export function DataDecorator<T = any>(
  setup: DataDecorator.Setup<T>
): DataDecorator<T> {
  let result: any;
  const actions: (() => unknown)[] = [];

  const cnvDate: DataDecorator.Converter = (field) => new Date(field);

  const cnvBool: DataDecorator.Converter = (field) => !!field;

  const cnvNumber: DataDecorator.Converter = (field) =>
    JS.is.number(field) ? field : Number.parseFloat(field);

  function cnvArray<TItem = unknown>(
    options?: DataDecorator.Array.Options<TItem>
  ): DataDecorator.Converter {
    const { sort } = options || {};

    return (field) => {
      if (!field) return [];

      if (sort) field.sort(JS.is.function(sort) ? sort : Compare.Structures);

      return field;
    };
  }

  function makeCnvEnum<TEnumTypeMap extends Enum.TypeMap>(
    enumeration: Enum<TEnumTypeMap>
  ): DataDecorator.Converter {
    return (field) =>
      enumeration.isUnion(field)
        ? enumeration.fromValue(field)
        : JS.is.string(field)
        ? enumeration.fromString(field)
        : NaN;
  }

  const Cnv = <TValue = any>(
    cnv: DataDecorator.Converter<TValue>,
    ...fieldNames: string[]
  ): void => {
    if (result)
      fieldNames.forEach((name) => {
        if (Object.keys(result).includes(name))
          result[name as keyof T] = cnv(result[name as keyof T]);
      });
  };

  const delEmpty = (...fieldNames: string[]): void => {
    if (result)
      fieldNames.forEach((name) => {
        const v = result[name as keyof T];
        if (!v && !JS.is.number(v)) delete result[name as keyof T];
      });
  };

  const dd: DataDecorator.Stage<T> = {
    print: (param: any) => {
      actions.push(() => {
        const { view, prefix }: DataDecorator.Stage.Print.Options<T> =
          JS.switch(param, {
            function: { view: param },
            string: { prefix: param },
            default: param,
          });

        console.log(
          `${prefix}%O${view ? ` %O` : ''}`,
          result,
          ...(view ? [view(result)] : [])
        );
      });
      return dd;
    },
    get Dates() {
      actions.push(() => Cnv(cnvDate, FIELDS.putdate, FIELDS.upddate));
      return dd;
    },
    get Activable() {
      actions.push(() => Cnv(cnvBool, FIELDS.active));
      return dd;
    },
    get Described() {
      actions.push(() => delEmpty(FIELDS.description));
      return dd;
    },
    get Systemic() {
      actions.push(() => Cnv(cnvBool, FIELDS.system));
      return dd;
    },
    get Select() {
      return dd.Dates;
    },
    bool: (...fieldNames: string[]) => {
      actions.push(() => Cnv(cnvBool, ...fieldNames));
      return dd;
    },
    number: (...fieldNames: string[]) => {
      actions.push(() => Cnv(cnvNumber, ...fieldNames));
      return dd;
    },
    noEmpty: (...fieldNames: string[]) => {
      actions.push(() => delEmpty(...fieldNames));
      return dd;
    },
    date: (...fieldNames: string[]) => {
      actions.push(() => Cnv(cnvDate, ...fieldNames));
      return dd;
    },
    array: <TItem = unknown>(
      param: string | DataDecorator.Array.Options<TItem>,
      ...fieldNames: string[]
    ) => {
      actions.push(() =>
        Cnv(
          cnvArray<TItem>(!JS.is.string(param) ? param : undefined),
          ...(JS.is.string(param) ? [param] : []),
          ...fieldNames
        )
      );

      return dd;
    },
    enum: <TEnumTypeMap extends Enum.TypeMap>(
      enumeration: Enum<TEnumTypeMap>,
      ...fieldNames: string[]
    ) => {
      actions.push(() => Cnv(makeCnvEnum(enumeration), ...fieldNames));
      return dd;
    },
    flags: (param: any, fieldOrOption?: any, ...fieldNames: string[]) => {
      actions.push(() => {
        if (!result) return;

        if (FlagsDecorator.is(param)) return param.expand(result);

        const hasOpt = JS.is.object(fieldOrOption);

        (hasOpt ? fieldNames : [fieldOrOption, ...fieldNames]).forEach(
          (field) => {
            const fc = hasOpt
              ? FlagsDecorator(param, { ...fieldOrOption, field })
              : FlagsDecorator(param, field);

            fc.expand(result);
          }
        );
      });
      return dd;
    },
    convert: (cnv: DataDecorator.Converter, ...fieldNames: string[]) => {
      actions.push(() => Cnv(cnv, ...fieldNames));
      return dd;
    },
    mapArray: (mapper: DataDecorator.Converter, ...fieldNames: string[]) => {
      actions.push(() =>
        Cnv(
          (arrayfield) => (arrayfield ? arrayfield.map(mapper) : []),
          ...fieldNames
        )
      );
      return dd;
    },
  };

  setup(dd);

  return (data: T): T => {
    if (!data) return data;

    result = { ...data };
    actions.forEach((action) => action());
    return result;
  };
}
