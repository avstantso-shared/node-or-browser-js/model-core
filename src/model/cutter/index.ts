import type { Structure, Cutter as _ } from '../../types/model';
import { isReadonly, isSimple, isHierarchical } from './is';

export type Func<TFrom, TTo> = _.Func<TFrom, TTo>;
export type Maximal<T extends Structure> = _.Maximal<T>;

export type ToMinimal<T extends Structure> = _.ToMinimal<T>;
export type ToInsert<T extends Structure> = _.ToInsert<T>;
export type ToUpdate<T extends Structure> = _.ToUpdate<T>;
export type ToInsertTree<T extends Structure> = _.ToInsertTree<T>;
export type ToUpdateTree<T extends Structure> = _.ToUpdateTree<T>;

export type Readonly<T extends Structure> = _.Readonly<T>;
export type Simple<T extends Structure> = _.Simple<T>;
export type Hierarchical<T extends Structure> = _.Hierarchical<T>;

export type Union<T extends Structure> = _.Union<T>;

export const Readonly = { Is: isReadonly };
export const Simple = { Is: isSimple };
export const Hierarchical = { Is: isHierarchical };
