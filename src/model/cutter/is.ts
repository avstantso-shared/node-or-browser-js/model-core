import { JS } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '../../types';

export function isReadonly<T extends Model.Structure = Model.Structure>(
  candidate: Model.Cutter.Union<T>
): candidate is Model.Cutter.Readonly<T> {
  return (
    null !== candidate && JS.is.object(candidate) && 'toMinimal' in candidate
  );
}

export function isSimple<T extends Model.Structure = Model.Structure>(
  candidate: Model.Cutter.Union<T>
): candidate is Model.Cutter.Simple<T> {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'toInsert' in candidate &&
    'toUpdate' in candidate
  );
}

export function isHierarchical<T extends Model.Structure = Model.Structure>(
  candidate: Model.Cutter.Union<T>
): candidate is Model.Cutter.Hierarchical<T> {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'toInsertTree' in candidate &&
    'toUpdateTree' in candidate
  );
}
