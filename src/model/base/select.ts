import { JS } from '@avstantso/node-or-browser-js--utils';

import * as Types from '../../types/model';
import { PutDate, UpdDate } from '../primitives';

import { Update } from './update';

/**
 * @summary entity fields set for select
 */
export type Select = Types.Select;

export namespace Select {
  export namespace Options {
    export type Serialized = Types.Select.Options.Serialized;
  }

  /**
   * @summary selection options for client and server
   */
  export type Options<
    TModel extends { Condition: unknown },
    TPagination extends Types.Pagination = Types.Pagination
  > = Types.Select.Options<TModel, TPagination>;
}

function isSelect<T extends Types.Structure = Types.Structure>(
  candidate: unknown
): candidate is T['Select'] {
  return (
    Update.Is<T>(candidate) && PutDate.Is(candidate) && UpdDate.Is(candidate)
  );
}
isSelect.Options = <
  TModel extends Types.Structure = Types.Structure,
  TPagination extends Types.Pagination = Types.Pagination
>(
  candidate: unknown
): candidate is Select.Options<TModel, TPagination> => {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    ('pagination' in candidate || 'search' in candidate)
  );
};

export const Select = {
  Is: isSelect,
};
