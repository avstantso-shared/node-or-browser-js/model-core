import * as Types from '../../types/model';
import { IDed, Versioned } from '../primitives';

/**
 * @summary entity fields set for update
 */
export type Update = Types.Update;

function isUpdate<T extends Types.Structure = Types.Structure>(
  candidate: unknown
): candidate is T['Update'] {
  return IDed.Is(candidate) && Versioned.Is(candidate);
}

export const Update = {
  Is: isUpdate,
};
