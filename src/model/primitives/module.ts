import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace Module {
  export type Provider = Types.Module.Provider;

  export type Equality<T extends Moduled> = Eq<T | string>;
}
export type Moduled = Types.Moduled;

export const Moduled = {
  ...AVStantso.Primitives.Utils.module,
};
