import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace Active {
  export type Provider = Types.Active.Provider;

  export type Equality<T extends Activable> = Eq<T | boolean>;
}

export type Activable = Types.Activable;

export const Activable = {
  ...AVStantso.Primitives.Utils.active,
};
