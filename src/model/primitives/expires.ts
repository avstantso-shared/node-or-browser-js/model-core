import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace Expires {
  export type Provider = Types.Expires.Provider;

  export type Equality<T extends Expirable> = Eq<T | Date>;
}

export type Expirable = Types.Expirable;

export const Expirable = {
  ...AVStantso.Primitives.Utils.expires,
};
