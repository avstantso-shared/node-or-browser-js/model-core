import { Generics, Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace System {
  export type Provider = Types.System.Provider;

  export type Filter = {
    /**
     * @summary Filter `=system` items only
     */
    <T>(system: boolean, list: T[]): T[];
    /**
     * @summary Filter `systemic` items only
     */
    systemic<T>(list: T[]): T[];

    /**
     * @summary Filter `!systemic` items only
     */
    noSystemic<T>(list: T[]): T[];
  };

  export type Equality<T extends Systemic> = Eq<T | boolean>;
}

export type Systemic = Types.Systemic;

const filter: System.Filter = <T>(system: boolean, list: T[]) =>
  list.filter(
    (item) => !!Generics.Cast.To<Systemic, T>(item).system === !!system
  );
filter.systemic = <T>(list: T[]): T[] => filter(true, list);
filter.noSystemic = <T>(list: T[]): T[] => filter(false, list);

export const System = {
  filter,
};

export const Systemic = {
  ...AVStantso.Primitives.Utils.system,
};
