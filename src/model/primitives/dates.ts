import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace PutDate {
  export type Provider = Types.PutDate.Provider;
}

export namespace UpdDate {
  export type Provider = Types.UpdDate.Provider;
}

export namespace Dates {
  export type Equality<T extends Dates> = Eq<T>;
}

export type Dates = Types.Dates;

export const PutDate = {
  ...AVStantso.Primitives.Utils.putdate,
};

export const UpdDate = {
  ...AVStantso.Primitives.Utils.upddate,
};
