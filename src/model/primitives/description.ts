import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export type Description = Types.Description;

export namespace Description {
  export type Provider = Types.Description.Provider;

  export type Equality<T extends Described> = Eq<T | string>;
}

export type Described = Types.Described;

export const Described = {
  ...AVStantso.Primitives.Utils.desctiption,
};
