import { Perform, Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export type ID = Types.ID;
export namespace ID {
  export type Provider = Types.ID.Provider;

  export type ToIDList = (...items: Perform<IDed | IDed[]>[]) => ID[];

  /**
   * @summary Create list filter for items ids only
   */
  export type Filter = (
    ...items: Perform<IDed | IDed[]>[]
  ) => <T extends IDed>(list: T[]) => T[];

  export type Equality<T extends IDed> = Eq<T | string>;
}
export type IDed = Types.IDed;

const toIDList: ID.ToIDList = (...items) =>
  Perform.List(...items).map((item) => item?.id);

const Filter: ID.Filter = (...items) => {
  return (list) => {
    const ids = toIDList(...items);
    return list.filter(({ id }) => ids.includes(id));
  };
};

export const ID = { toIDList, Filter };

export const IDed = {
  ...AVStantso.Primitives.Utils.id,
};
