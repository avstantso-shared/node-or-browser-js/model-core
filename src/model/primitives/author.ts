import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace Author {
  export namespace ID {
    export type Provider = Types.Author.ID.Provider;

    export type Equality<T extends Authored.ID> = Eq<T | Types.ID>;
  }

  export namespace Login {
    export type Provider = Types.Author.Login.Provider;

    export type Equality<T extends Authored.Login> = Eq<T | string>;
  }
}

export namespace Authored {
  export type ID = Types.Authored.ID;
  export type Login = Types.Authored.Login;
}

export const Authored = {
  ID: {
    ...AVStantso.Primitives.Utils.author_id,
  },
  Login: {
    ...AVStantso.Primitives.Utils.author,
  },
};
