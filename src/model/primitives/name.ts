import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export type Name = Types.Name;
export namespace Name {
  export type Provider = Types.Name.Provider;

  export type Equality<T extends Named> = Eq<T | Name>;
}
export type Named = Types.Named;

export const Named = {
  ...AVStantso.Primitives.Utils.name,
};
