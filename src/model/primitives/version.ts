import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export namespace Version {
  export type Provider = Types.Version.Provider;

  export type Equality<T extends Versioned> = Eq<T | number>;
}
export type Versioned = Types.Versioned;

export const Versioned = {
  ...AVStantso.Primitives.Utils.version,
};
