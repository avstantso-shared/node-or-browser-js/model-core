import { Equality as Eq } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../../types/model';

export type Login = Types.Login;
export namespace Login {
  export type Provider = Types.Login.Provider;

  export type Equality<T extends Logined> = Eq<T | Login>;
}
export type Logined = Types.Logined;

export const Logined = {
  ...AVStantso.Primitives.Utils.login,
};
