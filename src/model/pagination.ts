import { JS } from '@avstantso/node-or-browser-js--utils';
import * as Types from '../types/model';

type Model = { Select: unknown };

export namespace Pagination {
  export namespace Result {
    /**
     * @summary Total records count
     */
    export type Total = Types.Pagination.Result.Total;

    /**
     * @summary Pagination result object
     */
    export type AsObject<TModel extends Model> =
      Types.Pagination.Result.AsObject<TModel>;

    /**
     * @summary Pagination result convert function
     */
    export type Convert<
      TModel extends Model,
      TItem
    > = Types.Pagination.Result.Convert<TModel, TItem>;
  }

  /**
   * @summary Pagination result: total count, page items
   */
  export type Result<TModel extends Model> = Types.Pagination.Result<TModel>;
}

/**
 * @summary Pagination for client and server
 */
export type Pagination = Types.Pagination;

export function PaginationResult<TItem>(
  total: number,
  ...rows: TItem[]
): Pagination.Result<{ Select: TItem }> {
  return [total, rows];
}

function isPaginationResult<TModel extends Model = Model>(
  candidate: unknown
): candidate is Pagination.Result<TModel> {
  return (
    Array.isArray(candidate) &&
    2 === candidate.length &&
    (JS.is.number(candidate[0]) ||
      undefined === candidate[0] ||
      null === candidate[0]) &&
    Array.isArray(candidate[1])
  );
}

/**
 * @summary Initilize pagination object
 */
export function Pagination(num: number, size: number): Pagination {
  return { num, size };
}

function isPagination(candidate: unknown): candidate is Pagination {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'num' in candidate &&
    'size' in candidate
  );
}

PaginationResult.Is = isPaginationResult;

Pagination.Result = PaginationResult;
Pagination.Is = isPagination;
