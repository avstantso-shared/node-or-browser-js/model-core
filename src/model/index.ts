import * as Model from '../types/model';
import type * as Cutter from './cutter';

export {
  // A.V.Stantso:
  //  !!! Warning !!!
  //  Structure namespace not supported by TS Schema generator
  StructureKeys,
  StructureMetaKeys,
  StructureFrom,

  // A.V.Stantso: this generics work wrong in globally
  // Gen
  FieldInfo,
  FieldsInfo,
} from '../types/model';

export * from './base';
export * from './pagination';
export * from './primitives';
export { Cutter };

/**
 * @summary StructureMeta/Structure confuse protection
 */
export type StructureMeta = Model.StructureMeta &
  Record<Model.StructureKeys, never>;

/**
 * @summary StructureMeta/Structure confuse protection
 */
export type Structure = Model.Structure &
  Record<Model.StructureMetaKeys, never>;
