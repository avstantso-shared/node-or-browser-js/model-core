import type { JSONSchema7 } from 'json-schema';

import { ExtractGeneratedData } from '@avstantso/node-or-browser-js--utils';

import './_global';

import MODEL from './schemas/model.json';
import SITEMAP from './schemas/sitemap.json';
import SITE_METADATA from './schemas/site-metadata.json';
import * as Model from './export';

export { Model };

const extractGeneratedData = ExtractGeneratedData<JSONSchema7>();

export const SCHEMAS = {
  model: extractGeneratedData(MODEL),
  sitemap: extractGeneratedData(SITEMAP),
  siteMetadata: extractGeneratedData(SITE_METADATA),
};

export * from './types';
export * from './consts';
export * from './classes';
export * from './schemaUtils';
export * from './cutter';
export * from './data-decorator';
export * from './validation';
export * from './localization';
export * from './sitemap';
export * from './site-metadata';
