import _Ajv, { ErrorObject, Options } from 'ajv';
import addFormats from 'ajv-formats';
import addBase64 from 'ajv-base64';

export { ErrorObject };

export function Ajv(
  options: Options = {
    allErrors: true,
    strictTypes: false, // Allow Model.Name, Model.Description "maxLength" and etc.
  }
) {
  const ajv = new _Ajv(options);
  addFormats(ajv);
  addBase64(ajv);

  return ajv;
}
