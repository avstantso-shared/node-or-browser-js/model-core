import { JS, TS } from '@avstantso/node-or-browser-js--utils';

const Languages = TS.Type.Definer().Arr('en', 'ru');
const LanguagesStr = Languages as string[];

export namespace LocalString {
  export type Languages = typeof Languages;
  export type Language = TS.Array.KeyFrom<Languages>;
}

export type LocalString = Record<LocalString.Language, string>;

export const LocalString = {
  Languages,
  LanguagesStr,

  is: (param: unknown): param is LocalString => {
    if (!param || !JS.is.object(param)) return false;

    for (let i = 0; i < Languages.length; i++)
      if (!(Languages[i] in param)) return false;

    return true;
  },

  select: (ls: LocalString, lang: string) => ls[lang as LocalString.Language],

  compareLang: (a: string, b: string) =>
    LanguagesStr.indexOf(a) - LanguagesStr.indexOf(b),
};
