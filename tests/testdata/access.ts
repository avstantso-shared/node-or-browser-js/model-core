import type { Privilege } from '../testschemas/test';

const p1_id = 'c2433d8a-ec8a-11ec-bb53-00ff68bb875c';
const p2_id = 'c3433d8a-ec8a-11ec-bb53-00ff68bb875c';

export const READ_DATA_M: Privilege.Minimal = { name: 'READ_DATA' };
export const READ_DATA_S: Privilege.Select = {
  id: p1_id,
  version: 0,
  putdate: new Date(),
  upddate: new Date(),
  ...READ_DATA_M,
};

export const WRITE_DATA_M: Privilege.Minimal = { name: 'WRITE_DATA' };
export const WRITE_DATA_S: Privilege.Select = {
  id: p2_id,
  version: 0,
  putdate: new Date(),
  upddate: new Date(),
  ...WRITE_DATA_M,
};
