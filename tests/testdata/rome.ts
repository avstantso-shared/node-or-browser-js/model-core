import type { Empire } from '../testschemas/test';

const cr_id = 'bbcbbe3d-ec8a-11ec-bb53-00ff68bb875c';
const cm_id = 'c1433d8a-ec8a-11ec-bb53-00ff68bb875c';
const re_id = '9917d36e-ec8a-11ec-bb53-00ff68bb875c';

export const MILAN_M: Empire.City.Minimal = { name: 'milan', empire_id: re_id };
export const MILAN_I: Empire.City.Insert = {
  ...MILAN_M,
  square: 11,
  population: 5,
};
export const MILAN_U: Empire.City.Update = {
  id: cm_id,
  ...MILAN_I,
  version: 0,
};
export const MILAN_S: Empire.City.Select = {
  putdate: new Date(),
  upddate: new Date(),
  ...MILAN_U,
  populationDensity: MILAN_I.population / MILAN_I.square,
};
export const MILAN_C: Empire.City.Condition = {
  ...MILAN_M,
};

export const ROME_M: Empire.City.Minimal = { name: 'rome', empire_id: re_id };
export const ROME_I: Empire.City.Insert = {
  ...ROME_M,
  square: 11,
  population: 5,
};
export const ROME_U: Empire.City.Update = { id: cr_id, ...ROME_I, version: 0 };
export const ROME_S: Empire.City.Select = {
  putdate: new Date(),
  upddate: new Date(),
  ...ROME_U,
  populationDensity: ROME_I.population / ROME_I.square,
};
export const ROME_C: Empire.City.Condition = {
  ...ROME_M,
};

export const ROME_EMPIRE_M: Empire.Minimal = { name: 'rome_empire' };
export const ROME_EMPIRE_I: Empire.Insert = {
  ...ROME_EMPIRE_M,
  square: 156,
};
export const ROME_EMPIRE_IT: Empire.InsertTree = {
  ...ROME_EMPIRE_I,
  cities: [ROME_I, MILAN_I],
};
export const ROME_EMPIRE_U: Empire.Update = {
  id: re_id,
  ...ROME_EMPIRE_I,
  version: 0,
};
export const ROME_EMPIRE_UT: Empire.UpdateTree = {
  ...ROME_EMPIRE_U,
  cities: [ROME_U, MILAN_U],
};
export const ROME_EMPIRE_S: Empire.Select = {
  ...ROME_EMPIRE_UT,
  putdate: new Date(),
  upddate: new Date(),
  cities: [ROME_S, MILAN_S],
};
export const ROME_EMPIRE_C: Empire.Condition = {
  ...ROME_EMPIRE_M,
};
