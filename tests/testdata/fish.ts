import type { LifeForm } from '../testschemas/test';

const fish_id = 'c6aa11f8-ec89-11ec-bb53-00ff68bb875c';

export const FISH_M: LifeForm.Minimal = { name: 'fish' };
export const FISH_I: LifeForm.Insert = { ...FISH_M, size: 15 };
export const FISH_U: LifeForm.Update = {
  id: fish_id,
  version: 0,
  ...FISH_I,
};
export const FISH_S: LifeForm.Select = {
  putdate: new Date(),
  upddate: new Date(),
  ...FISH_U,
};
export const FISH_C: LifeForm.Condition = {
  ...FISH_M,
};
