import 'jest';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

import { Model } from '../../../src';

// A.V.Stantso: You can uncomment this to check that these methods do not exist
//
// const readonlyArray: ReadonlyArray<{ id: string }> = [];
// readonlyArray.by.id.sort // Property 'sort' does not exist on type...

class CommonTestError extends Error {
  constructor(message: string = 'Common test error') {
    super(message);

    Object.setPrototypeOf(this, CommonTestError.prototype);
  }

  static is(error: unknown): error is CommonTestError {
    return JS.is.error(this, error);
  }

  static expection(test: () => unknown) {
    try {
      test();
      fail('No error');
    } catch (e) {
      if (CommonTestError.is(e)) expect(e).toBeTruthy();
      else throw e;
    }
  }
}

describe('Array', () => {
  const data = [
    { id: 'a', putdate: new Date(), version: 1, name: 'Z' },
    { id: 'b', putdate: new Date(), version: 3, name: 'Y' },
    { id: 'c', putdate: new Date(), version: 8, name: 'X' },
  ];
  data[0].putdate.setHours(1);
  data[1].putdate.setHours(-8);
  data[2].putdate.setHours(14);

  function Data(...indexes: [number, number, number]) {
    const [i0, i1, i2] = indexes;
    return [data[i0], data[i1], data[i2]];
  }

  describe('Immutable', () => {
    describe('Common', () => {
      function CommonTest(primitive: unknown) {
        const {
          meta,
          Extract,
        }: AVStantso.Primitives.Meta.Provider &
          AVStantso.Primitives.Utils<any> = Generics.Cast.To(primitive);

        const key = meta.K as keyof (typeof data)[0];
        it(key, () => {
          const provider = (value: any): any => JS.set.raw({}, key, value);

          const [a, b, c] = data.map(Extract) as [any, any, any];

          const x: any =
            a instanceof Date
              ? new Date()
              : JS.is.string(a)
              ? ''
              : JS.is.number(a)
              ? 0
              : undefined;

          if (data[0].hasOwnProperty(key)) {
            expect(data.by(key).find(a)).toBe(data[0]);
            expect(data.by(key).find.neq(a)).toBe(data[1]);
            expect(data.by(key).find(x)).toBeUndefined();
            expect(data.by(key).find(provider(a))).toBe(data[0]);
            expect(data.by(key).find(provider(null))).toBeUndefined();

            expect(data.by(key).findIndex(b)).toBe(1);
            expect(data.by(key).findIndex.neq(b)).toBe(0);
            expect(data.by(key).findIndex(x)).toBe(-1);
            expect(data.by(key).findIndex(provider(b))).toBe(1);
            expect(data.by(key).findIndex(provider(undefined))).toBe(-1);

            expect(data.by(key).some(c)).toBe(true);
            expect(data.by(key).some.neq(c)).toBe(true);
            expect(data.by(key).some(x)).toBe(false);
            expect(data.by(key).some(provider(c))).toBe(true);
            expect(data.by(key).some(provider(x))).toBe(false);

            expect(data.by(key).filter(a)).toStrictEqual([data[0]]);
            expect(data.by(key).filter.neq(a)).toStrictEqual([
              data[1],
              data[2],
            ]);
            expect(data.by(key).filter(x)).toStrictEqual([]);
            expect(data.by(key).filter(provider(a))).toStrictEqual([data[0]]);
            expect(data.by(key).filter(provider(null))).toStrictEqual([]);

            expect(data.by(key).extract()).toStrictEqual([a, b, c]);
          } else {
            const o = {
              defaults: () => {
                throw new CommonTestError();
              },
            };

            const e = CommonTestError.expection;

            e(() => data.by(key).find(a, o));
            e(() => data.by(key).findIndex(a, o));
            e(() => data.by(key).some(a, o));
            e(() => data.by(key).every(a, o));
            e(() => data.by(key).filter(a, o));
            e(() => data.by(key).extract(o));
          }
        });
      }

      [
        Model.Activable,
        Model.Authored.ID,
        Model.Authored.Login,
        Model.PutDate,
        Model.UpdDate,
        Model.Described,
        Model.Expirable,
        Model.IDed,
        Model.Logined,
        Model.Moduled,
        Model.Named,
        Model.Systemic,
        Model.Versioned,
      ].forEach(CommonTest);
    });

    describe('Manually', () => {
      it('id', () => {
        const [a, b, c] = data.map(({ id }) => id) as [
          Model.ID,
          Model.ID,
          Model.ID
        ];
        const x = '';

        expect(data.by.id.find(a)).toBe(data[0]);
        expect(data.by.id.find(x)).toBeUndefined();
        expect(data.by.id.find({ id: a })).toBe(data[0]);
        expect(data.by.id.find({ id: null })).toBeUndefined();

        expect(data.by.id.findIndex(b)).toBe(1);
        expect(data.by.id.findIndex(x)).toBe(-1);
        expect(data.by.id.findIndex({ id: b })).toBe(1);
        expect(data.by.id.findIndex({ id: undefined })).toBe(-1);

        expect(data.by.id.some(c)).toBe(true);
        expect(data.by.id.some(x)).toBe(false);
        expect(data.by.id.some({ id: c })).toBe(true);
        expect(data.by.id.some({ id: x })).toBe(false);

        expect(data.by.id.filter(a)).toStrictEqual([data[0]]);
        expect(data.by.id.filter(x)).toStrictEqual([]);
        expect(data.by.id.filter({ id: a })).toStrictEqual([data[0]]);
        expect(data.by.id.filter({ id: null })).toStrictEqual([]);

        expect(data.by.id.extract()).toStrictEqual([a, b, c]);

        expect(data.by.id.count('a')).toBe(1);
      });

      it('name', () => {
        const [a, b, c] = data.map(({ name }) => name) as [
          Model.Name,
          Model.Name,
          Model.Name
        ];
        const x = '';

        expect(data.by.name.find(a)).toBe(data[0]);
        expect(data.by.name.find(x)).toBeUndefined();
        expect(data.by.name.find({ name: a })).toBe(data[0]);
        expect(data.by.name.find({ name: null })).toBeUndefined();

        expect(data.by.name.findIndex(b)).toBe(1);
        expect(data.by.name.findIndex(x)).toBe(-1);
        expect(data.by.name.findIndex({ name: b })).toBe(1);
        expect(data.by.name.findIndex({ name: undefined })).toBe(-1);

        expect(data.by.name.some(c)).toBe(true);
        expect(data.by.name.some(x)).toBe(false);
        expect(data.by.name.some({ name: c })).toBe(true);
        expect(data.by.name.some({ name: x })).toBe(false);

        expect(data.by.name.filter(a)).toStrictEqual([data[0]]);
        expect(data.by.name.filter(x)).toStrictEqual([]);
        expect(data.by.name.filter({ name: a })).toStrictEqual([data[0]]);
        expect(data.by.name.filter({ name: null })).toStrictEqual([]);

        expect(data.by.name.extract()).toStrictEqual([a, b, c]);

        expect(data.by.name.count('Y')).toBe(1);
      });

      it('putdate', () => {
        const [a, b, c] = data.map(({ putdate }) => new Date(putdate)) as [
          Date,
          Date,
          Date
        ];
        const x = new Date();

        expect(data.by.putdate.find(a)).toBe(data[0]);
        expect(data.by.putdate.find(x)).toBeUndefined();
        expect(data.by.putdate.find({ putdate: a })).toBe(data[0]);
        expect(data.by.putdate.find({ putdate: null })).toBeUndefined();

        expect(data.by.putdate.findIndex(b)).toBe(1);
        expect(data.by.putdate.findIndex(x)).toBe(-1);
        expect(data.by.putdate.findIndex({ putdate: b })).toBe(1);
        expect(data.by.putdate.findIndex({ putdate: undefined })).toBe(-1);

        expect(data.by.putdate.some(c)).toBe(true);
        expect(data.by.putdate.some(x)).toBe(false);
        expect(data.by.putdate.some({ putdate: c })).toBe(true);
        expect(data.by.putdate.some({ putdate: x })).toBe(false);

        expect(data.by.putdate.filter(a)).toStrictEqual([data[0]]);
        expect(data.by.putdate.filter(x)).toStrictEqual([]);
        expect(data.by.putdate.filter({ putdate: a })).toStrictEqual([data[0]]);
        expect(data.by.putdate.filter({ putdate: null })).toStrictEqual([]);

        expect(data.by.putdate.extract()).toStrictEqual([a, b, c]);

        expect(data.by.putdate.count(b)).toBe(1);
      });

      it('version', () => {
        const [a, b, c] = data.map(({ version }) => version) as [
          number,
          number,
          number
        ];
        const x = 0;

        expect(data.by.version.find(a)).toBe(data[0]);
        expect(data.by.version.find(x)).toBeUndefined();
        expect(data.by.version.find({ version: a })).toBe(data[0]);
        expect(data.by.version.find({ version: null })).toBeUndefined();

        expect(data.by.version.findIndex(b)).toBe(1);
        expect(data.by.version.findIndex(x)).toBe(-1);
        expect(data.by.version.findIndex({ version: b })).toBe(1);
        expect(data.by.version.findIndex({ version: undefined })).toBe(-1);

        expect(data.by.version.some(c)).toBe(true);
        expect(data.by.version.some(x)).toBe(false);
        expect(data.by.version.some({ version: c })).toBe(true);
        expect(data.by.version.some({ version: x })).toBe(false);

        expect(data.by.version.filter(a)).toStrictEqual([data[0]]);
        expect(data.by.version.filter(x)).toStrictEqual([]);
        expect(data.by.version.filter({ version: a })).toStrictEqual([data[0]]);
        expect(data.by.version.filter({ version: null })).toStrictEqual([]);

        expect(data.by.version.extract()).toStrictEqual([a, b, c]);

        expect(data.by.version.count(3)).toBe(1);
      });
    });
  });

  describe('Mutable', () => {
    describe(`sort`, () => {
      const o = { nullsFirst: false };

      it(`id`, () => {
        const u = { id: undefined as string };
        const n = { id: null as string };

        expect([...data, u, n].by.id.sort()).toStrictEqual([
          u,
          n,
          ...Data(0, 1, 2),
        ]);
        expect([...data, u, n].by.id.sort.desc()).toStrictEqual([
          ...Data(2, 1, 0),
          n,
          u,
        ]);

        expect([...data, u, n].by.id.sort(o)).toStrictEqual([
          ...Data(0, 1, 2),
          u,
          n,
        ]);
        expect([...data, u, n].by.id.sort.desc(o)).toStrictEqual([
          n,
          u,
          ...Data(2, 1, 0),
        ]);
      });

      it(`putdate`, () => {
        const u = { putdate: undefined as Date };
        const n = { putdate: null as Date };

        expect([...data, u, n].by.putdate.sort()).toStrictEqual([
          u,
          n,
          ...Data(1, 0, 2),
        ]);
        expect([...data, u, n].by.putdate.sort.desc()).toStrictEqual([
          ...Data(2, 0, 1),
          n,
          u,
        ]);

        expect([...data, u, n].by.putdate.sort(o)).toStrictEqual([
          ...Data(1, 0, 2),
          u,
          n,
        ]);
        expect([...data, u, n].by.putdate.sort.desc(o)).toStrictEqual([
          n,
          u,
          ...Data(2, 0, 1),
        ]);
      });

      it(`version`, () => {
        const u = { version: undefined as number };
        const n = { version: null as number };
        const nn = { version: NaN };

        expect([...data, u, nn, n].by.version.sort()).toStrictEqual([
          u,
          n,
          nn,
          ...Data(0, 1, 2),
        ]);
        expect([...data, u, nn, n].by.version.sort.desc()).toStrictEqual([
          ...Data(2, 1, 0),
          nn,
          n,
          u,
        ]);

        expect([...data, u, nn, n].by.version.sort(o)).toStrictEqual([
          ...Data(0, 1, 2),
          u,
          n,
          nn,
        ]);
        expect([...data, u, nn, n].by.version.sort.desc(o)).toStrictEqual([
          nn,
          n,
          u,
          ...Data(2, 1, 0),
        ]);
      });

      it(`name`, () => {
        expect([...data].by.name.sort()).toStrictEqual(Data(2, 1, 0));
        expect([...data].by.name.sort.desc()).toStrictEqual(Data(0, 1, 2));
      });
    });
  });
});
