import 'jest';

import { Model } from '../../src';

const ided = { id: '123' };
const versioned = { version: 8 };
const update = { ...ided, ...versioned };
const putdated = { putdate: new Date() };
const upddated = { upddate: new Date() };
const select = { ...update, ...putdated, ...upddated };

describe('Base-Types', () => {
  describe('Update', () => {
    it('Is', () => {
      const { Is } = Model.Update;

      expect(Is({})).toBe(false);

      expect(Is({ ...ided })).toBe(false);
      expect(Is({ ...versioned })).toBe(false);

      expect(Is(update)).toBe(true);
    });
  });

  describe('Select', () => {
    it('Is', () => {
      const { Is } = Model.Select;

      expect(Is({})).toBe(false);

      expect(Is({ ...update })).toBe(false);
      expect(Is({ ...putdated })).toBe(false);
      expect(Is({ ...upddated })).toBe(false);

      expect(Is({ ...update, ...putdated })).toBe(false);
      expect(Is({ ...update, ...upddated })).toBe(false);
      expect(Is({ ...putdated, ...upddated })).toBe(false);

      expect(Is(select)).toBe(true);
    });
  });
});
