import 'jest';
import { SiteMap } from '../../src';

describe('SiteMap', () => {
  describe('Url', () => {
    describe('Changefreq', () => {
      describe('asDate', () => {
        const { asMilliseconds } = SiteMap.Url.Changefreq;

        const m4 = new Date(2000, 4, 4);

        function mk([value, expected]: [string, Date?]) {
          it(`${value}` || `<empty>`, () => {
            const vActual = asMilliseconds(value as SiteMap.Url.Changefreq);

            if ('never' === value) expect(vActual).toBe(Infinity);
            else if (!value) expect(vActual).toBeNull();
            else
              expect(
                `${new Date(
                  +m4 + asMilliseconds(value as SiteMap.Url.Changefreq)
                )}`
              ).toBe(`${expected}`);
          });
        }

        const Expections: Record<SiteMap.Url.Changefreq, Date> = {
          always: m4,
          hourly: new Date(2000, 4, 4, 1),
          daily: new Date(2000, 4, 5),
          weekly: new Date(2000, 4, 11),
          monthly: new Date(2000, 5, 4),
          yearly: new Date(2001, 4, 4),
          never: new Date(Infinity),
        };

        describe('Valid', () => {
          Object.entries(Expections).forEach(mk);
        });

        describe('Empty', () => {
          mk([null]);
          mk([undefined]);
          mk(['']);
        });
      });
    });
  });
});
