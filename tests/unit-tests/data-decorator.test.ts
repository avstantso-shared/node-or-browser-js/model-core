import 'jest';
import { Rus } from '@avstantso/node-or-browser-js--utils';
import { DataDecorator } from '../../src';

describe('dataDecorator', () => {
  describe('one-operation', () => {
    it('Dates', () => {
      const d = DataDecorator((dd) => dd.Dates);

      const putdate = new Date().toString();
      const upddate = new Date().toString();
      expect(d({ putdate, upddate })).toStrictEqual({
        putdate: new Date(putdate),
        upddate: new Date(upddate),
      });
    });

    it('Select', () => {
      const d = DataDecorator((dd) => dd.Select);

      const putdate = new Date().toString();
      const upddate = new Date().toString();
      expect(d({ putdate, upddate })).toStrictEqual({
        putdate: new Date(putdate),
        upddate: new Date(upddate),
      });
    });

    it('Activable', () => {
      const d = DataDecorator((dd) => dd.Activable);

      expect(d({ active: 1 })).toStrictEqual({
        active: true,
      });

      expect(d({ active: 0 })).toStrictEqual({
        active: false,
      });
    });

    it('Described', () => {
      const d = DataDecorator((dd) => dd.Described);

      const description = 'This is the description';
      expect(d({ description })).toStrictEqual({
        description,
      });

      expect(d({ description: '' })).toStrictEqual({});
      expect(d({ description: null })).toStrictEqual({});
      expect(d({ description: undefined })).toStrictEqual({});
    });

    it('Systemic', () => {
      const d = DataDecorator((dd) => dd.Systemic);

      expect(d({ system: 1 })).toStrictEqual({
        system: true,
      });

      expect(d({ system: 0 })).toStrictEqual({
        system: false,
      });
    });

    it('bool', () => {
      const d = DataDecorator((dd) => dd.bool('a', 'b', 'c'));

      expect(d({ a: 1, b: 0, c: 1, d: 0, e: 1 })).toStrictEqual({
        a: true,
        b: false,
        c: true,
        d: 0,
        e: 1,
      });
    });

    it('number', () => {
      const d = DataDecorator((dd) => dd.number('a', 'b', 'c'));

      expect(
        d({ a: '1.1', b: '0.2', c: '1.3', d: '0.4', e: '1.5' })
      ).toStrictEqual({
        a: 1.1,
        b: 0.2,
        c: 1.3,
        d: '0.4',
        e: '1.5',
      });
    });

    it('noEmpty', () => {
      const d = DataDecorator((dd) => dd.noEmpty('a', 'b', 'c', 'd', 'f'));

      expect(
        d({ a: '', b: undefined, c: 0, d: null, e: 0.0, g: 1, h: undefined })
      ).toStrictEqual({
        c: 0,
        e: 0.0,
        g: 1,
        h: undefined,
      });
    });

    it('date', () => {
      const d = DataDecorator((dd) => dd.date('a', 'b'));

      const a = new Date().toString();
      const b = new Date().toString();
      const c = new Date().toString();

      expect(d({ a, b, c, d: '0.4', e: '1.5' })).toStrictEqual({
        a: new Date(a),
        b: new Date(b),
        c,
        d: '0.4',
        e: '1.5',
      });
    });

    it('array', () => {
      const d = DataDecorator((dd) => dd.array('a', 'b', 'c', 'd'));

      expect(d({ a: [1], b: [], c: undefined, d: null, e: 1 })).toStrictEqual({
        a: [1],
        b: [],
        c: [],
        d: [],
        e: 1,
      });
    });

    it('enum', () => {
      const d = DataDecorator((dd) =>
        dd.enum(Rus.Case, 'a', 'b', 'c', 'd', 'e')
      );

      expect(
        d({
          a: 'genitive',
          b: Rus.Case._.nominative,
          c: 5,
          d: Rus.Case.dative,
          e: {},
          f: 'accusative',
          g: 6,
        })
      ).toStrictEqual({
        a: Rus.Case.genitive,
        b: Rus.Case.nominative,
        c: Rus.Case.instrumental,
        d: Rus.Case.dative,
        e: NaN,
        f: 'accusative',
        g: 6,
      });
    });

    it('convert', () => {
      const d = DataDecorator((dd) =>
        dd.convert((x) => JSON.stringify(x), 'a', 'b', 'c', 'd')
      );

      expect(d({ a: [1], b: {}, c: undefined, d: null, e: 1 })).toStrictEqual({
        a: '[1]',
        b: '{}',
        c: undefined,
        d: 'null',
        e: 1,
      });
    });

    it('mapArray', () => {
      const d = DataDecorator((dd) => dd.mapArray((x) => x + 1, 'a'));

      expect(
        d({ a: [1, 2, 3], b: {}, c: undefined, d: null, e: 1 })
      ).toStrictEqual({
        a: [2, 3, 4],
        b: {},
        c: undefined,
        d: null,
        e: 1,
      });
    });
  });

  describe('many-operations', () => {
    it('complex', () => {
      const d = DataDecorator((dd) =>
        dd.Select.Activable.Systemic.Described.array('items').bool('confirmed')
      );

      const putdate = new Date().toString();
      const upddate = new Date().toString();

      expect(
        d({
          putdate,
          upddate,
          active: 1,
          system: 0,
          description: null,
          items: null,
          confirmed: 1,
        })
      ).toStrictEqual({
        putdate: new Date(putdate),
        upddate: new Date(upddate),
        active: true,
        system: false,
        items: [],
        confirmed: true,
      });
    });
  });
});
