import 'jest';
import { ModelCutterError, validateBySchema } from '../../src';
import { SCHEMA } from '../testschemas/test';
import { Privilege, LifeForm, Empire, Fields } from '../testschemas/test';
import {
  READ_DATA_M,
  READ_DATA_S,
  WRITE_DATA_M,
  WRITE_DATA_S,
} from '../testdata/access';
import { FISH_M, FISH_I, FISH_U, FISH_S, FISH_C } from '../testdata/fish';
import {
  MILAN_M,
  MILAN_I,
  MILAN_U,
  MILAN_S,
  MILAN_C,
  ROME_M,
  ROME_I,
  ROME_U,
  ROME_S,
  ROME_C,
  ROME_EMPIRE_M,
  ROME_EMPIRE_I,
  ROME_EMPIRE_IT,
  ROME_EMPIRE_U,
  ROME_EMPIRE_UT,
  ROME_EMPIRE_S,
  ROME_EMPIRE_C,
} from '../testdata/rome';

describe('core-model', () => {
  describe('cutters', () => {
    it('readonly', () => {
      expect(Privilege.Minimal.Cutter(READ_DATA_S)).toStrictEqual(READ_DATA_M);
      expect(Privilege.Minimal.Cutter(READ_DATA_M)).toStrictEqual(READ_DATA_M);

      expect(Privilege.Minimal.Cutter(WRITE_DATA_S)).toStrictEqual(
        WRITE_DATA_M
      );
      expect(Privilege.Minimal.Cutter(WRITE_DATA_M)).toStrictEqual(
        WRITE_DATA_M
      );
    });

    it('simple', () => {
      expect(LifeForm.Update.Cutter(FISH_S)).toStrictEqual(FISH_U);
      expect(LifeForm.Update.Cutter(FISH_U)).toStrictEqual(FISH_U);

      expect(LifeForm.Insert.Cutter(FISH_S)).toStrictEqual(FISH_I);
      expect(LifeForm.Insert.Cutter(FISH_U)).toStrictEqual(FISH_I);
      expect(LifeForm.Insert.Cutter(FISH_I)).toStrictEqual(FISH_I);

      expect(LifeForm.Minimal.Cutter(FISH_S)).toStrictEqual(FISH_M);
      expect(LifeForm.Minimal.Cutter(FISH_U)).toStrictEqual(FISH_M);
      expect(LifeForm.Minimal.Cutter(FISH_I)).toStrictEqual(FISH_M);
      expect(LifeForm.Minimal.Cutter(FISH_M)).toStrictEqual(FISH_M);
    });

    it('hierarchy', () => {
      expect(Empire.UpdateTree.Cutter(ROME_EMPIRE_S)).toStrictEqual(
        ROME_EMPIRE_UT
      );
      expect(Empire.UpdateTree.Cutter(ROME_EMPIRE_UT)).toStrictEqual(
        ROME_EMPIRE_UT
      );

      expect(Empire.Update.Cutter(ROME_EMPIRE_S)).toStrictEqual(ROME_EMPIRE_U);
      expect(Empire.Update.Cutter(ROME_EMPIRE_UT)).toStrictEqual(ROME_EMPIRE_U);
      expect(Empire.Update.Cutter(ROME_EMPIRE_U)).toStrictEqual(ROME_EMPIRE_U);

      expect(Empire.InsertTree.Cutter(ROME_EMPIRE_S)).toStrictEqual(
        ROME_EMPIRE_IT
      );
      expect(Empire.InsertTree.Cutter(ROME_EMPIRE_UT)).toStrictEqual(
        ROME_EMPIRE_IT
      );
      expect(Empire.InsertTree.Cutter(ROME_EMPIRE_IT)).toStrictEqual(
        ROME_EMPIRE_IT
      );

      expect(Empire.Insert.Cutter(ROME_EMPIRE_S)).toStrictEqual(ROME_EMPIRE_I);
      expect(Empire.Insert.Cutter(ROME_EMPIRE_UT)).toStrictEqual(ROME_EMPIRE_I);
      expect(Empire.Insert.Cutter(ROME_EMPIRE_U)).toStrictEqual(ROME_EMPIRE_I);
      expect(Empire.Insert.Cutter(ROME_EMPIRE_IT)).toStrictEqual(ROME_EMPIRE_I);
      expect(Empire.Insert.Cutter(ROME_EMPIRE_I)).toStrictEqual(ROME_EMPIRE_I);

      expect(Empire.Minimal.Cutter(ROME_EMPIRE_S)).toStrictEqual(ROME_EMPIRE_M);
      expect(Empire.Minimal.Cutter(ROME_EMPIRE_UT)).toStrictEqual(
        ROME_EMPIRE_M
      );
      expect(Empire.Minimal.Cutter(ROME_EMPIRE_U)).toStrictEqual(ROME_EMPIRE_M);
      expect(Empire.Minimal.Cutter(ROME_EMPIRE_IT)).toStrictEqual(
        ROME_EMPIRE_M
      );
      expect(Empire.Minimal.Cutter(ROME_EMPIRE_I)).toStrictEqual(ROME_EMPIRE_M);
      expect(Empire.Minimal.Cutter(ROME_EMPIRE_M)).toStrictEqual(ROME_EMPIRE_M);

      expect(Empire.City.Minimal.Cutter(MILAN_S)).toStrictEqual(MILAN_M);
    });

    it('error', () => {
      const data: any = { ...READ_DATA_S, name: 15 };

      try {
        Privilege.Minimal.Cutter(data);
      } catch (error) {
        expect(ModelCutterError.is(error)).toBe(true);

        if (ModelCutterError.is(error)) {
          expect(error.data).toStrictEqual(data);
          expect(error.errors.length).toBe(1);

          const { schemaPath: ignoredSchemaPath, ...error0 } = error.errors[0];

          expect(error0).toStrictEqual({
            instancePath: '/name',
            keyword: 'type',
            params: { type: 'string' },
            message: 'must be string',
          });
        }
      }
    });
  });

  it('validation', () => {
    expect(validateBySchema(MILAN_S, SCHEMA.City.Select)).toStrictEqual({
      isValid: true,
    });
  });

  it('fields', () => {
    const [entitiesFields, allFields] = [
      { ...Privilege.Fields, ...LifeForm.Fields, ...Empire.Fields },
      Fields,
    ].map((arr) => Object.values(arr).sort((a, b) => a.localeCompare(b)));

    expect(entitiesFields.length).toBe(allFields.length);
    expect(entitiesFields).toStrictEqual(allFields);
  });
});
