import 'jest';

import { PrimitiveIt } from '@avstantso/node-js--model-core-test';

import { Model } from '../../src';

import TS = AVStantso.TS;
import Primitives = AVStantso.Primitives;

describe('Primitives', () => {
  const DFs = Primitives.Meta.DefineFragments;
  const DF = Primitives.Meta.DefineFragment;

  describe('Meta', () => {
    describe('Define', () => {
      const Expected = (() => {
        const FragmentsLow = {
          IDed: {
            id: {
              type: 'string',
            },
          },
          Named: {
            name: {
              type: 'string',
              length: {
                min: 3,
                max: 30,
              },
              pattern: '^[A-Za-z][A-Za-z0-9_]{2,29}$',
            },
          },
          Logined: {
            login: {
              type: 'string',
            },
          },
        };

        const Fragments = {
          ...FragmentsLow,
          Activable: {
            active: {
              type: 'boolean',
              optional: true,
              default: true,
            },
          },
          'Authored.ID': {
            author_id: {
              type: 'string',
            },
          },
          Described: {
            desctiption: {
              type: 'string',
              length: {
                max: 256,
              },
              optional: true,
            },
          },
          Dates: {
            putdate: {
              type: 'Date',
            },
            upddate: {
              type: 'Date',
            },
          },
          Versioned: {
            version: {
              type: 'number',
              default: 0,
            },
          },
        };

        const FieldsLow = {
          ...FragmentsLow.IDed,
          ...FragmentsLow.Named,
          ...FragmentsLow.Logined,
        };

        const Fields = {
          ...FieldsLow,

          ...Fragments.Activable,
          ...Fragments['Authored.ID'],
          ...Fragments.Described,
          ...Fragments.Dates,
          ...Fragments.Versioned,

          user: {
            type: 'string',
          },
          token: {
            type: 'Buffer',
            length: 64,
            pattern: '^(d|[A-Fa-f]){64}$',
          },
          dice: {
            type: 'number',
            min: 1,
            max: 6,
          },
          one_two_three: {
            type: 'string',
            values: ['one', 'two', 'three'],
          },
          accept_encoding: {
            type: 'string',
            optional: true,
          },
        };

        const ExternalFields = {
          life: {
            type: 'handler',
            external: true,
            optional: true,
          },
        };

        return { FragmentsLow, FieldsLow, Fragments, Fields, ExternalFields };
      })();

      describe('Bad-typization', () => {
        function testFF(lowOnly?: boolean) {
          const FragmentsLow = DFs((f) => ({
            IDed: { id: f.string },
            Named: {
              name: f.string({
                length: { min: 3, max: 30 },
                pattern: '^[A-Za-z][A-Za-z0-9_]{2,29}$',
              }),
            },
            Logined: { login: f('string') },
          }));
          expect(FragmentsLow).toStrictEqual(Expected.FragmentsLow);

          const FieldsLow = { ...TS.Type.flat(FragmentsLow) };
          expect(FieldsLow).toStrictEqual(Expected.FieldsLow);
          if (lowOnly) return;

          const Fragments = {
            ...DFs.For({ Fields: FieldsLow })(
              ({ boolean, number, string, Date, id }) => ({
                Activable: {
                  active: boolean({ optional: true, default: true }),
                },

                'Authored.ID': { author_id: id },

                Described: {
                  desctiption: string({ optional: true, length: { max: 256 } }),
                },

                Dates: { putdate: Date, upddate: Date },

                Versioned: { version: number({ default: 0 }) },
              })
            ),
            ...FragmentsLow,
          };
          expect(Fragments).toStrictEqual(Expected.Fragments);

          const Fields = {
            ...DF.For({ Fields: FieldsLow })(
              ({ number, string, Buffer, login }) => ({
                user: login,
                token: Buffer({ length: 64, pattern: '^(d|[A-Fa-f]){64}$' }),
                dice: number({ min: 1, max: 6 }),
                one_two_three: string({
                  values: ['one', 'two', 'three', 'one'],
                }), // duplicates ignored
                accept_encoding: string.compatible<string | string[]>()({
                  optional: true,
                }),
              })
            ),

            ...TS.Type.flat(Fragments),
            ...FieldsLow,
          };
          expect(Fields).toStrictEqual(Expected.Fields);
        }

        it(`bad-low-fragments-fields`, () => testFF(true));

        it(`bad-all-fragments-fields`, () => testFF(false));

        it(`bad-external-fields`, () => {
          const ExternalFields = DF.For({ External: { handler: () => 42 } })(
            (f) => ({ life: f('handler')({ optional: true }) })
          );
          expect(ExternalFields).toStrictEqual(Expected.ExternalFields);
        });
      });

      describe('Good-typization', () => {
        function testFF(lowOnly?: boolean) {
          const FragmentsLow = DFs((f) => ({
            IDed: { id: f.string },
            Named: {
              name: f.string
                .length(3, 30)
                .pattern('^[A-Za-z][A-Za-z0-9_]{2,29}$'),
            },
            Logined: { login: f('string') },
          }));
          expect(FragmentsLow).toStrictEqual(Expected.FragmentsLow);

          const FieldsLow = { ...TS.Type.flat(FragmentsLow) };
          expect(FieldsLow).toStrictEqual(Expected.FieldsLow);
          if (lowOnly) return;

          const Fragments = {
            ...DFs.For({ Fields: FieldsLow })(
              ({ boolean, number, string, Date, id }) => ({
                Activable: { active: boolean.optional.default(true) },

                'Authored.ID': { author_id: id },

                Described: { desctiption: string.optional.length.max(256) },

                Dates: { putdate: Date, upddate: Date },

                Versioned: { version: number.default(0) },
              })
            ),
            ...FragmentsLow,
          };
          expect(Fragments).toStrictEqual(Expected.Fragments);

          const Fields = {
            ...DF.For({ Fields: FieldsLow })(
              ({ number, string, Buffer, login }) => ({
                user: login,
                token: Buffer.length(64).pattern('^(d|[A-Fa-f]){64}$'),
                dice: number.min(1).max(6),
                one_two_three: string.values('one', 'two', 'three', 'one'), // duplicates ignored
                accept_encoding: string.compatible<string | string[]>()
                  .optional,
              })
            ),

            ...TS.Type.flat(Fragments),
            ...FieldsLow,
          };
          expect(Fields).toStrictEqual(Expected.Fields);
        }

        it(`good-low-fragments-fields`, () => testFF(true));

        it(`good-all-fragments-fields`, () => testFF(false));

        it(`good-external-fields`, () => {
          const ExternalFields = DF.For({ External: { handler: () => 42 } })(
            ({ handler }) => ({ life: handler.optional })
          );
          expect(ExternalFields).toStrictEqual(Expected.ExternalFields);
        });
      });
    });
  });

  describe('globals', () => {
    it('field-map-extend-conflict', () => {
      // No conflict
      expect(
        Primitives.Field.Map._extend({ active: { type: 'boolean' } })
      ).toBeUndefined();

      try {
        Primitives.Field.Map._extend({ active: { type: 'string' } });

        fail('No errors');
      } catch (e: any) {
        expect(e.message || '').toMatch(`Primitives field "active"`);

        expect(e.context).toStrictEqual({
          field: 'active',
          old: { type: 'boolean' },
          new: { type: 'string' },
        });
      }
    });

    it('fragment-map-extend-conflict', () => {
      // No conflict
      expect(
        Primitives.Fragment.Map._extend({
          Activable: { active: { type: 'boolean' } },
        })
      ).toBeUndefined();

      try {
        Primitives.Fragment.Map._extend({
          Activable: { active: { type: 'string' } },
        });

        fail('No errors');
      } catch (e: any) {
        expect(e.message || '').toMatch(`Primitives fragment "Activable"`);

        expect(e.context).toStrictEqual({
          fragment: 'Activable',
          field: 'active',
          old: { active: { type: 'boolean' } },
          new: { active: { type: 'string' } },
        });
      }
    });
  });

  [
    Model.Activable,
    Model.Authored.ID,
    Model.Authored.Login,
    Model.PutDate,
    Model.UpdDate,
    Model.Described,
    Model.Expirable,
    Model.IDed,
    Model.Logined,
    Model.Moduled,
    Model.Named,
    Model.Systemic,
    Model.Versioned,
  ].forEach(PrimitiveIt);
});
