import type { Model } from '../../src/types';

/**
 * @implements Model.StructureFrom<Privilege.Meta>
 */
export interface Privilege extends Model.Structure {
  Minimal: Model.Minimal & Privilege.Meta['minimal'];
  Select: Model.Select & Privilege.Meta['minimal'];
  Condition: Partial<Privilege.Meta['minimal']>;
}

export namespace Privilege {
  export interface Meta extends Model.StructureMeta {
    minimal: Model.Named;
  }

  export type Minimal = Privilege['Minimal'];
  export type Select = Privilege['Select'];
  export type Condition = Privilege['Condition'];
}
