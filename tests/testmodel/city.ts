import type { Model } from '../../src/types';

/**
 * @implements Model.StructureFrom<City.Meta>
 */
export interface City extends Model.Structure {
  Master: Omit<City.Meta['master'], 'entity'>;
  Minimal: Model.Minimal &
    Omit<City.Meta['master'], 'entity'> &
    City.Meta['minimal'];
  Insert: Model.Insert &
    Omit<City.Meta['master'], 'entity'> &
    City.Meta['insert'];
  InsertDetail: Model.Insert &
    Partial<Omit<City.Meta['master'], 'entity'>> &
    City.Meta['insert'];
  Update: Model.Update &
    Omit<City.Meta['master'], 'entity'> &
    City.Meta['insert'];
  UpdateDetail: Model.Update &
    Partial<Omit<City.Meta['master'], 'entity'>> &
    City.Meta['insert'];
  Select: Model.Select &
    Omit<City.Meta['master'], 'entity'> &
    City.Meta['minimal'] &
    City.Meta['insert'] &
    City.Meta['select'];
  Condition: Partial<
    City.Meta['minimal'] & City.Meta['insert'] & City.Meta['select']
  >;
}

export namespace City {
  export interface Meta extends Model.StructureMeta {
    master: { entity: 'Empire'; empire_id: Model.ID };
    minimal: Model.Named;
    insert: Model.Named & {
      square: number;
      population: number;
    };
    select: { populationDensity: number };
  }

  export type Minimal = City['Minimal'];
  export type Insert = City['Insert'];
  export type InsertDetail = City['InsertDetail'];
  export type Update = City['Update'];
  export type UpdateDetail = City['UpdateDetail'];
  export type Select = City['Select'];
  export type Condition = City['Condition'];
}
