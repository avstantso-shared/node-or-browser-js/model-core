import type { Model } from '../../src/types';
import type { City } from './city';

/**
 * @implements Model.StructureFrom<Empire.Meta>
 */
export interface Empire extends Model.Structure {
  Minimal: Model.Minimal & Empire.Meta['minimal'];
  Insert: Model.Insert & Empire.Meta['insert'];
  InsertTree: Model.Insert & Empire.Meta['insert'] & Empire.Meta['insertTree'];
  Update: Model.Update & Empire.Meta['insert'];
  UpdateTree: Model.Update & Empire.Meta['insert'] & Empire.Meta['updateTree'];
  Select: Model.Select &
    Empire.Meta['minimal'] &
    Empire.Meta['insert'] &
    Empire.Meta['select'];
  Condition: Partial<Empire.Meta['minimal'] & Empire.Meta['insert']>;
}

export namespace Empire {
  export interface Meta extends Model.StructureMeta {
    minimal: Model.Named;
    insert: Model.Named & {
      square: number;
    };
    insertTree: { cities: City.InsertDetail[] };
    updateTree: { cities: City.UpdateDetail[] };
    select: { cities: City.Select[] };
  }

  export type Minimal = Empire['Minimal'];
  export type Insert = Empire['Insert'];
  export type InsertTree = Empire['InsertTree'];
  export type Update = Empire['Update'];
  export type UpdateTree = Empire['UpdateTree'];
  export type Select = Empire['Select'];
  export type Condition = Empire['Condition'];
}
