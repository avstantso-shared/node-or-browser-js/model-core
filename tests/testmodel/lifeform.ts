import type { Model } from '../../src/types';

/**
 * @implements Model.StructureFrom<LifeForm.Meta>
 */
export interface LifeForm extends Model.Structure {
  Minimal: Model.Minimal & LifeForm.Meta['minimal'];
  Insert: Model.Insert & LifeForm.Meta['insert'];
  Update: Model.Update & LifeForm.Meta['insert'];
  Select: Model.Select & LifeForm.Meta['minimal'] & LifeForm.Meta['insert'];
  Condition: Partial<LifeForm.Meta['minimal'] & LifeForm.Meta['insert']>;
}

export namespace LifeForm {
  export interface Meta extends Model.StructureMeta {
    minimal: Model.Named;
    insert: Model.Named & { size: number };
  }

  export type Minimal = LifeForm['Minimal'];
  export type Insert = LifeForm['Insert'];
  export type Update = LifeForm['Update'];
  export type Select = LifeForm['Select'];
  export type Condition = LifeForm['Condition'];
}
